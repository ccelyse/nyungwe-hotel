<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

//    public function authenticate(Request $request)
//    {
//
//        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
//
//            $user_get_role = User::select('name', 'name as user_role')->where('email', $request['email'])->value('role');
//            dd($user_get_role);
////            switch ($lastE) {
////                case "user":
////                    return redirect()->intended('TDashboard');
////                    break;
////                case "Sales Department":
////                    return redirect()->route('backend.financial.SDashboard');
////                case "Marketing Department":
////                    return redirect()->route('backend.marketing.MDashboard');
////                default:
////                    return redirect()->back();
////                    break;
////            }
////        }else{
////            return redirect()->back();
////        }
//        }
//    }

    use AuthenticatesUsers;
//
//    /**
//     * Where to redirect users after login.
//     *
//     * @var string
//     */
//    protected $redirectTo = '/Dashboard';
//
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
