<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/mail', function () {
//    \Mail::to(User::find(68))->send(new OrderEmail($getOrders,$getguestPhonenumberbold));
//});

Route::get('/',['as'=>'welcome','uses'=>'FrontendController@Home']);
Route::get('Accommodation',['as'=>'Accommodation','uses'=>'FrontendController@Accommodation']);
Route::get('AccommodationsMore',['as'=>'AccommodationsMore','uses'=>'FrontendController@AccommodationsMore']);
Route::get('AboutUs',['as'=>'AboutUs','uses'=>'FrontendController@AboutUs']);
Route::get('Facilities',['as'=>'Facilities','uses'=>'FrontendController@Facilities']);
Route::get('ContactUs',['as'=>'ContactUs','uses'=>'FrontendController@ContactUs']);
Route::get('/Apply',['as'=>'Apply','uses'=>'FrontendController@Apply']);
Route::post('/Apply_',['as'=>'Apply_','uses'=>'FrontendController@Apply_']);


Route::get('/ContactUs', ['as'=>'ContactUs','uses'=>'FrontendController@ContactUs']);
Route::get('/login', ['as'=>'backend.Login','uses'=>'FrontendController@Login']);
Route::post('/LoginApp',['as'=>'login','uses'=>'BackendController@SignIn_']);

//Auth::routes();
Route::group(['middleware' => ['auth']], function () {
//        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::get('/Accommodations', ['as' => 'backend.Accommodations', 'uses' => 'BackendController@Accommodations']);
        Route::post('/AddAccommodation', ['as' => 'backend.AddAccommodation', 'uses' => 'BackendController@AddAccommodation']);
        Route::get('/DeleteAccommodation', ['as' => 'backend.DeleteAccommodation', 'uses' => 'BackendController@DeleteAccommodation']);
        Route::post('/EditAccommodation', ['as' => 'backend.EditAccommodation', 'uses' => 'BackendController@EditAccommodation']);
        Route::post('/getLogout', ['as' => 'backend.getLogout', 'uses' => 'BackendController@getLogout']);
//        Route::get('/AddaBrand', ['as' => 'backend.AddaBrand', 'uses' => 'BackendController@AddaBrand']);
        Route::get('/AddInsurance', ['as' => 'backend.AddInsurance', 'uses' => 'BackendController@AddInsurance']);
        Route::post('/AddInsurance_', ['as' => 'backend.AddInsurance_', 'uses' => 'BackendController@AddInsurance_']);
        Route::get('/ListInsurance', ['as' => 'backend.ListInsurance', 'uses' => 'BackendController@ListInsurance']);
        Route::get('/DeleteBrand', ['as' => 'backend.DeleteBrand', 'uses' => 'BackendController@DeleteBrand']);
        Route::get('/EditInsurance', ['as' => 'backend.EditInsurance', 'uses' => 'BackendController@EditInsurance']);
        Route::post('/EditInsurance_', ['as' => 'backend.EditInsurance_', 'uses' => 'BackendController@EditInsurance_']);

        Route::get('/AddDepartment', ['as' => 'backend.AddDepartment', 'uses' => 'BackendController@AddDepartment']);
        Route::post('/AddDepartment_', ['as' => 'backend.AddDepartment_', 'uses' => 'BackendController@AddDepartment_']);
        Route::get('/DeleteDepartment', ['as' => 'backend.DeleteDepartment', 'uses' => 'BackendController@DeleteDepartment']);
        Route::get('/EditDepartment', ['as' => 'backend.EditDepartment', 'uses' => 'BackendController@EditDepartment']);
        Route::post('/EditDepartment_', ['as' => 'backend.EditDepartment_', 'uses' => 'BackendController@EditDepartment_']);


        Route::get('/AddMedicines', ['as' => 'backend.AddMedicines', 'uses' => 'BackendController@AddMedicines']);
        Route::post('/AddMedicines_', ['as' => 'backend.AddMedicines_', 'uses' => 'BackendController@AddMedicines_']);
        Route::get('/EditMedicine', ['as' => 'backend.EditMedicine', 'uses' => 'BackendController@EditMedicine']);
        Route::post('/EditMedicine_', ['as' => 'backend.EditMedicine_', 'uses' => 'BackendController@EditMedicine_']);
        Route::get('/DeleteMedicine', ['as' => 'backend.DeleteMedicine', 'uses' => 'BackendController@DeleteMedicine']);


        Route::get('/LaboratoryTest', ['as' => 'backend.LaboratoryTest', 'uses' => 'BackendController@LaboratoryTest']);
        Route::post('/LaboratoryTest_', ['as' => 'backend.LaboratoryTest_', 'uses' => 'BackendController@LaboratoryTest_']);
        Route::get('/EditLaboTest', ['as' => 'backend.EditLaboTest', 'uses' => 'BackendController@EditLaboTest']);
        Route::post('/EditLaboTest_', ['as' => 'backend.EditLaboTest_', 'uses' => 'BackendController@EditLaboTest_']);
        Route::get('/DeleteLaboratoryTest', ['as' => 'backend.DeleteLaboratoryTest', 'uses' => 'BackendController@DeleteLaboratoryTest']);
        Route::get('/LaboratoryPatientHistory', ['as' => 'backend.LaboratoryPatientHistory', 'uses' => 'BackendController@LaboratoryPatientHistory']);
        Route::get('/LaboOtherResults', ['as' => 'backend.LaboOtherResults', 'uses' => 'BackendController@LaboOtherResults']);
        Route::post('/AddOtherResultsPatients', ['as' => 'backend.AddOtherResultsPatients', 'uses' => 'BackendController@AddOtherResultsPatients']);
        Route::get('/DeleteOtherResultsPatients', ['as' => 'backend.DeleteOtherResultsPatients', 'uses' => 'BackendController@DeleteOtherResultsPatients']);
        Route::post('/EditOtherResultsPatients', ['as' => 'backend.EditOtherResultsPatients', 'uses' => 'BackendController@EditOtherResultsPatients']);
        Route::get('/LaboratoryNFS', ['as' => 'backend.LaboratoryNFS', 'uses' => 'BackendController@LaboratoryNFS']);
        Route::post('/AddOtherResultsPatientsNFS', ['as' => 'backend.AddOtherResultsPatientsNFS', 'uses' => 'BackendController@AddOtherResultsPatientsNFS']);
        Route::post('/EditOtherResultsPatientsNFS', ['as' => 'backend.EditOtherResultsPatientsNFS', 'uses' => 'BackendController@EditOtherResultsPatientsNFS']);
        Route::get('/DeleteOtherResultsPatientsNFS', ['as' => 'backend.DeleteOtherResultsPatientsNFS', 'uses' => 'BackendController@DeleteOtherResultsPatientsNFS']);

        Route::get('/Rooms', ['as' => 'backend.Rooms', 'uses' => 'BackendController@Rooms']);
        Route::post('/Rooms_', ['as' => 'backend.Rooms_', 'uses' => 'BackendController@Rooms_']);
        Route::get('/EditRoomInfo', ['as' => 'backend.EditRoomInfo', 'uses' => 'BackendController@EditRoomInfo']);
        Route::post('/EditRoomInfo_', ['as' => 'backend.EditRoomInfo_', 'uses' => 'BackendController@EditRoomInfo_']);
        Route::get('/DeleteRoomInfo', ['as' => 'backend.DeleteRoomInfo', 'uses' => 'BackendController@DeleteRoomInfo']);
        Route::get('/RoomPrice', ['as' => 'backend.RoomPrice', 'uses' => 'BackendController@RoomPrice']);
        Route::post('/RoomPrice_', ['as' => 'backend.RoomPrice_', 'uses' => 'BackendController@RoomPrice_']);
        Route::get('/EditRoomPrice', ['as' => 'backend.EditRoomPrice', 'uses' => 'BackendController@EditRoomPrice']);
        Route::post('/EditRoomPrice_', ['as' => 'backend.EditRoomPrice_', 'uses' => 'BackendController@EditRoomPrice_']);
        Route::get('/DeleteRoomPrices', ['as' => 'backend.DeleteRoomPrices', 'uses' => 'BackendController@DeleteRoomPrices']);

        Route::get('/Reception', ['as' => 'backend.Reception', 'uses' => 'BackendController@Reception']);
        Route::post('/Reception_', ['as' => 'backend.Reception_', 'uses' => 'BackendController@Reception_']);
        Route::get('/EditPatient', ['as' => 'backend.EditPatient', 'uses' => 'BackendController@EditPatient']);
        Route::post('/EditPatient_', ['as' => 'backend.EditPatient_', 'uses' => 'BackendController@EditPatient_']);
        Route::get('/DeletePatient', ['as' => 'backend.DeletePatient', 'uses' => 'BackendController@DeletePatient']);
        Route::get('/ReceptionDashboard', ['as' => 'backend.ReceptionDashboard', 'uses' => 'BackendController@ReceptionDashboard']);
        Route::get('/ReceptionAccountant', ['as' => 'backend.ReceptionAccountant', 'uses' => 'BackendController@ReceptionAccountant']);
        Route::get('/ReceptionAccountPatient', ['as' => 'backend.ReceptionAccountPatient', 'uses' => 'BackendController@ReceptionAccountPatient']);
        Route::post('/NewRecordHistory', ['as' => 'backend.NewRecordHistory', 'uses' => 'BackendController@NewRecordHistory']);
        Route::get('/DoctorPatientHistoryConsult', ['as' => 'backend.DoctorPatientHistoryConsult', 'uses' => 'BackendController@DoctorPatientHistoryConsult']);
        Route::get('/DoctorOperationPatientRecordHistory', ['as' => 'backend.DoctorOperationPatientRecordHistory', 'uses' => 'BackendController@DoctorOperationPatientRecordHistory']);
        Route::get('/DoctorPatientHistoryOperation', ['as' => 'backend.DoctorPatientHistoryOperation', 'uses' => 'BackendController@DoctorPatientHistoryOperation']);
        Route::post('/ReceptionAccountantFilter', ['as' => 'backend.ReceptionAccountantFilter', 'uses' => 'BackendController@ReceptionAccountantFilter']);



        Route::get('/NurseDashboard', ['as' => 'backend.NurseDashboard', 'uses' => 'BackendController@NurseDashboard']);
        Route::get('/NurseConsultation', ['as' => 'backend.NurseConsultation', 'uses' => 'BackendController@NurseConsultation']);
        Route::get('/PatientHistoryRecord', ['as' => 'backend.PatientHistoryRecord', 'uses' => 'BackendController@PatientHistoryRecord']);
        Route::get('/NurseOperationPatientRecordHistory', ['as' => 'backend.NurseOperationPatientRecordHistory', 'uses' => 'BackendController@NurseOperationPatientRecordHistory']);
        Route::get('/NurseConsultationPatient', ['as' => 'backend.NurseConsultationPatient', 'uses' => 'BackendController@NurseConsultationPatient']);
        Route::post('/AddPatientMeasurement', ['as' => 'backend.AddPatientMeasurement', 'uses' => 'BackendController@AddPatientMeasurement']);
        Route::get('/PatientOperations', ['as' => 'backend.PatientOperations', 'uses' => 'BackendController@PatientOperations']);
        Route::post('/EditPatientMeasurement', ['as' => 'backend.EditPatientMeasurement', 'uses' => 'BackendController@EditPatientMeasurement']);
        Route::get('/DeletePatientMeasurement', ['as' => 'backend.DeletePatientMeasurement', 'uses' => 'BackendController@DeletePatientMeasurement']);
        Route::get('/LaboratoryDashboard', ['as' => 'backend.LaboratoryDashboard', 'uses' => 'BackendController@LaboratoryDashboard']);

        Route::get('/Nurse', ['as' => 'backend.Nurse', 'uses' => 'BackendController@Nurse']);
        Route::get('/NursePatient', ['as' => 'backend.NursePatient', 'uses' => 'BackendController@NursePatient']);
        Route::get('/PatientConsultaton', ['as' => 'backend.PatientConsultaton', 'uses' => 'BackendController@PatientConsultaton']);
        Route::post('/PatientConsultaton_', ['as' => 'backend.PatientConsultaton_', 'uses' => 'BackendController@PatientConsultaton_']);
        Route::get('/DeletePatientConsultaton', ['as' => 'backend.DeletePatientConsultaton', 'uses' => 'BackendController@DeletePatientConsultaton']);
        Route::post('/EditPatientConsultaton_', ['as' => 'backend.EditPatientConsultaton_', 'uses' => 'BackendController@EditPatientConsultaton_']);
        Route::post('/PatientCharges', ['as' => 'backend.PatientCharges', 'uses' => 'BackendController@PatientCharges']);
        Route::post('/PatientChargesNurse', ['as' => 'backend.PatientChargesNurse', 'uses' => 'BackendController@PatientChargesNurse']);
        Route::post('/PatientChargeSelectMedicine', ['as' => 'backend.PatientChargeSelectMedicine', 'uses' => 'BackendController@PatientChargeSelectMedicine']);
        Route::post('/PatientCharges_', ['as' => 'backend.PatientCharges_', 'uses' => 'BackendController@PatientCharges_']);
        Route::post('/AddPatientCharges', ['as' => 'backend.AddPatientCharges', 'uses' => 'BackendController@AddPatientCharges']);
        Route::post('/AddAppointmentDoctors', ['as' => 'backend.AddAppointmentDoctors', 'uses' => 'BackendController@AddAppointmentDoctors']);
        Route::post('/EditAddAppointmentDoctors', ['as' => 'backend.EditAddAppointmentDoctors', 'uses' => 'BackendController@EditAddAppointmentDoctors']);
        Route::get('/DeleteAddAppointmentDoctors', ['as' => 'backend.DeleteAddAppointmentDoctors', 'uses' => 'BackendController@DeleteAddAppointmentDoctors']);
        Route::post('/AddAppointmentLabo', ['as' => 'backend.AddAppointmentLabo', 'uses' => 'BackendController@AddAppointmentLabo']);
        Route::post('/EditAppointmentLabo', ['as' => 'backend.EditAppointmentLabo', 'uses' => 'BackendController@EditAppointmentLabo']);
        Route::get('/DeleteAddAppointmentLabo', ['as' => 'backend.DeleteAddAppointmentLabo', 'uses' => 'BackendController@DeleteAddAppointmentLabo']);
//        Route::get('/EditPatientCharges', ['as' => 'backend.EditPatientCharges', 'uses' => 'BackendController@EditPatientCharges']);
        Route::get('/DeletePatientCharges', ['as' => 'backend.DeletePatientCharges', 'uses' => 'BackendController@DeletePatientCharges']);



        Route::get('/LaboratoryTech', ['as' => 'backend.Laboratory', 'uses' => 'BackendController@LaboratoryTech']);
        Route::get('/LaboratoryTechTest', ['as' => 'backend.LaboratoryTechTest', 'uses' => 'BackendController@LaboratoryTechTest']);

        Route::get('/Accountant', ['as' => 'backend.Accountant', 'uses' => 'BackendController@Accountant']);
        Route::get('/AccountantPatientPay', ['as' => 'backend.AccountantPatientPay', 'uses' => 'BackendController@AccountantPatientPay']);
        Route::get('/AccountantPatientHistory', ['as' => 'backend.AccountantPatientHistory', 'uses' => 'BackendController@AccountantPatientHistory']);
        Route::get('/AccountantPatient', ['as' => 'backend.AccountantPatient', 'uses' => 'BackendController@AccountantPatient']);
        Route::get('/AccountantViewMore', ['as' => 'backend.AccountantViewMore', 'uses' => 'BackendController@AccountantViewMore']);
        Route::post('/AccountPatientClear', ['as' => 'backend.AccountPatientClear', 'uses' => 'BackendController@AccountPatientClear']);
        Route::post('/AccountantUpdatePaymentStatus', ['as' => 'backend.AccountantUpdatePaymentStatus', 'uses' => 'BackendController@AccountantUpdatePaymentStatus']);
        Route::get('/AccountantSales', ['as' => 'backend.AccountantSales', 'uses' => 'BackendController@AccountantSales']);
        Route::post('/AccountantSalesFilter', ['as' => 'backend.AccountantSalesFilter', 'uses' => 'BackendController@AccountantSalesFilter']);
        Route::get('/HospitalizeSales', ['as' => 'backend.HospitalizeSales', 'uses' => 'BackendController@HospitalizeSales']);
        Route::post('/HospitalizeAccountantSalesFilter', ['as' => 'backend.HospitalizeAccountantSalesFilter', 'uses' => 'BackendController@HospitalizeAccountantSalesFilter']);


        Route::get('/HospitalizationDashboard', ['as' => 'backend.HospitalizationDashboard', 'uses' => 'BackendController@HospitalizationDashboard']);
        Route::get('/HospitalizePatientHistory', ['as' => 'backend.HospitalizePatientHistory', 'uses' => 'BackendController@HospitalizePatientHistory']);
        Route::get('/DeleteHospitalizePatient', ['as' => 'backend.DeleteHospitalizePatient', 'uses' => 'BackendController@DeleteHospitalizePatient']);
        Route::get('/Hospitalize', ['as' => 'backend.Hospitalize', 'uses' => 'BackendController@Hospitalize']);
        Route::get('/HospitalizePatient', ['as' => 'backend.HospitalizePatient', 'uses' => 'BackendController@HospitalizePatient']);
        Route::post('/GetRoomLevelAjax', ['as' => 'backend.GetRoomLevelAjax', 'uses' => 'BackendController@GetRoomLevelAjax']);
        Route::post('/GetRoomPriceAjax', ['as' => 'backend.GetRoomPriceAjax', 'uses' => 'BackendController@GetRoomPriceAjax']);
        Route::post('/AddHospitalizePatient', ['as' => 'backend.AddHospitalizePatient', 'uses' => 'BackendController@AddHospitalizePatient']);
        Route::get('/HospitalizeReport', ['as' => 'backend.HospitalizeReport', 'uses' => 'BackendController@HospitalizeReport']);
        Route::post('/HospitalizeReportFilter', ['as' => 'backend.HospitalizeReportFilter', 'uses' => 'BackendController@HospitalizeReportFilter']);


        Route::get('/DoctorDashboard', ['as' => 'backend.DoctorDashboard', 'uses' => 'BackendController@DoctorDashboard']);
        Route::get('/Doctor', ['as' => 'backend.Doctor', 'uses' => 'BackendController@Doctor']);
        Route::get('/DoctorConsultation', ['as' => 'backend.Doctor', 'uses' => 'BackendController@DoctorConsultation']);
        Route::get('/DoctorPatientHistoryRecord', ['as' => 'backend.DoctorPatientHistoryRecord', 'uses' => 'BackendController@DoctorPatientHistoryRecord']);
        Route::get('/DoctorOperations', ['as' => 'backend.Doctor', 'uses' => 'BackendController@DoctorOperations']);
        Route::get('/DoctorPatient', ['as' => 'backend.DoctorPatient', 'uses' => 'BackendController@DoctorPatient']);
        Route::post('/SOAP', ['as' => 'backend.SOAP', 'uses' => 'BackendController@SOAP']);
        Route::post('/SOAPEdit', ['as' => 'backend.SOAPEdit', 'uses' => 'BackendController@SOAPEdit']);
        Route::post('/ConfirmAppointment', ['as' => 'backend.ConfirmAppointment', 'uses' => 'BackendController@ConfirmAppointment']);
        Route::post('/ConfirmAppointmentEdit', ['as' => 'backend.ConfirmAppointmentEdit', 'uses' => 'BackendController@ConfirmAppointmentEdit']);
        Route::get('/ConfirmAppointmentDelete', ['as' => 'backend.ConfirmAppointmentDelete', 'uses' => 'BackendController@ConfirmAppointmentDelete']);


        Route::get('/AccountantDashboard', ['as' => 'backend.AccountantDashboard', 'uses' => 'BackendController@AccountantDashboard']);
        Route::get('/Accountant', ['as' => 'backend.Accountant', 'uses' => 'BackendController@Accountant']);
        Route::get('/InvoiceTobeGenerated', ['as' => 'backend.InvoiceTobeGenerated', 'uses' => 'BackendController@InvoiceTobeGenerated']);
        Route::get('/InvoiceHospitalize', ['as' => 'backend.InvoiceHospitalize', 'uses' => 'BackendController@InvoiceHospitalize']);
        Route::get('/PayedSales', ['as' => 'backend.PayedSales', 'uses' => 'BackendController@PayedSales']);
        Route::get('/DeletePayedSales', ['as' => 'backend.DeletePayedSales', 'uses' => 'BackendController@DeletePayedSales']);
        Route::get('/AccountList', ['as' => 'backend.AccountList', 'uses' => 'BackendController@AccountList']);
        Route::get('/CreateAccount', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount']);
        Route::post('/CreateAccount_', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount_']);

        Route::get('/ShowSession', ['as' => 'backend.ShowSession', 'uses' => 'BackendController@ShowSession']);
        Route::get('/SetSession', ['as' => 'backend.SetSession', 'uses' => 'BackendController@SetSession']);
        Route::get('/deleteSessionData', ['as' => 'backend.deleteSessionData', 'uses' => 'BackendController@deleteSessionData']);
//        Route::get('PdfView',array('as'=>'backend.PdfView','uses'=>'BackendController@PdfView'));
//        Route::get('/PdfView_', ['as' => 'backend.PdfView_', 'uses' => 'BackendController@PdfView_']);
//        Route::get('/ViewMore', ['as' => 'backend.ViewMore', 'uses' => 'BackendController@ViewMore']);
////        Route::get('/PdfView','BackendController@export_pdf');
//        Route::post('/AddAttractions_', ['as' => 'backend.AddAttractions_', 'uses' => 'BackendController@AddAttractions_']);
});





