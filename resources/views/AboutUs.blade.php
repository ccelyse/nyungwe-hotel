@extends('layouts.master')

@section('title', 'Nyungwe Hotel')

@section('content')


    <div class="slider-height" style="background-color: #000;">

        <div id="slides" class="home-slider superslides full-width">
            <div class="slides-container">

                <div class="slide-item">
                    <img class="hero-image" src="cmsImages/DJI_0031.jpg" alt="">
                    <div class="container">
                        <div class="verticalCenter slide-content">
                            <div class="verticalInner">
                                <h2>About Us</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-item">
                    <img class="hero-image" src="cmsImages/DJI_0031.jpg" alt="">
                    <div class="container">
                        <div class="verticalCenter slide-content">
                            <div class="verticalInner">
                                <h2>About Us</h2>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <nav class="slides-navigation">
                <a href="rooms.html#" class="next"></a>
                <a href="rooms.html#" class="prev"></a>
            </nav>
            <a class="nextblock wow fadeInDown"  data-wow-delay="0.5s" href="rooms.html#Next-block">
                <div class="verticalCenter">
                    <div class="verticalInner">
                        <span>Scroll</span>
                        <i class="fa fa-angle-down"></i>
                    </div>
                </div>
            </a>
        </div>
        <!--Slider End-->

    </div>



    <div id="Next-block" class="room-content-w full-width">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Location</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>Located in Buvungira Cell of Bushekeri Sector of Nyamasheke district of the Western Province of Rwanda, Nyungwe Top View Hill Hotel is owned by a Rwandese entrepreneur and conservationist.In operation since February 2011, the hotel is composed of two distinct compartments. The main area consists of a large, two-tiered rondavel (see photo below) with a phenomenally high roof. Although a rondavel is usually a traditionally built round house, this building offers a rather more modern take on the theme. The ground floor, where you find the reception, is furnished with a few modern angular sofas and walls adorned with traditional Rwandan murals.Circular wicker lights hang from the ceiling and dark tiled stairs lead you up to a mezzanine balcony. Whereas the basement cubicle hosts administration offices, the reception, kitchen, store and the laundry, it is the second level that hosts dining area and the bar both with tables and chairs set on a rather out-of-place black-tiled floor and a sweeping veranda with all-weather tables and chairs of steel and moulded materials. The views from here are stunning and on clear days you can even see Lake Kivu and the volcanoes of far North Rwanda and the Eastern of Democratic Republic of Congo. The bar on this level is entirely decorated from bottle tops, giving it a bright but simple look.</p>

                    <p>As the names suggests Nyungwe Top View Hill Hotel is built on the top of one of the hills positioning it to have the most beautiful views in the Nyungwe Forest area. Located five minutes' drive off a main road, uphill, the hotel has 12 cottages arranged into two rows, one at the sunrise and the other at the sunset side. On each side, there is a row of six red-brick cottages ranged northward. Each of these double or twin cottages houses a lounge area at the front, with terracotta-tiled floors, sofa, chair and coffee table, and a fireplace which can be lit for you on cold evenings. The bedrooms are quite sparse, furnished with a bed, hanging space for clothes, hair dressing table and amazing well decorated spacious and clean bathroom.</p>

                </div>

                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Attraction</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>The hotel is located on the vicinity of Nyungwe National Park which
                        harbours a number of tourism attractions including various primates that
                        are already accustomed to human presence including chimpanzees, black
                        and white colobus, mangabey, and unique plant species including orchids
                        making it special for visitors. Also the park is well located as it is situated
                        at the Congo Nile water Divide. Thus attractions including a trial denoted
                        the Congo-Nile has been created to enable nature ventures to experience
                        this wonder.</p>

                    <p>The forest is also home for endangered bird species making it a suitable
                        destination for birders. Waterfalls and a newly constructed canopy walk
                        way and interpretation centre are also some of the key tourists pulling
                        factors.</p>

                    <p>Furthermore, the hotel is well positioned to accommodate those willing to
                        visit the low land Gorilla of Kahuzi Biega National Park in DRC.</p>
                </div>

                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Accessibility</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>The hotel is located at less than two kilometres from the main high way
                        road from Kigali to Bukavu. This makes its accessibility easier to travellers.</p>

                    <p>Additionally, the hotel is located at 30 minutes drive from the shore of
                        Lake Kivu. Being an attraction itself, Lake Kivu has been identified as an
                        important medium for transport of tourists who not only want to enjoy
                        scenic view of the lake but also who want to link mountain gorilla visit to
                        Nyungwe National Park and its surrounding areas. For this purpose there
                        is a boat to facilitate the Lake Kivu circuit which connects Rusizi to Rubavu
                        via Nyamasheke and Karongi thrice a week.</p>

                    <p>The site can also be accessible by air as it is located at 45 minutes drive
                        from Kamembe airport where a daily schedule is covered by Rwanda Air.</p>
                    <p>The hotel is also suitably located to accommodate people willing to do
                        business in Cyangugu and neighboring countries including the Democratic
                        Republic of Congo and Burundi. It is located at 45 minutes drive from
                        Bukavu (DRC) and 3 hours to Bujumbura (Burundi).</p>
                </div>
                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Uniqueness</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>The site has an incomparable location view. From East, the site enjoys the
                        view of different parts of Nyungwe National Park forest canopy and a suite
                        of gentle hills. Northwards, the site has the view of Lake Kivu and a chain
                        of extinct and active volcanoes including Karisimbi, Mikeno, Nyamuragira
                        and Nyiragongo. On West, the site has marvellous view of terraces from
                        surrounding communities, Lake Kivu and a chain of mountain of Kahuzi
                        Biega National Park. On South the site has the view of Nyungwe forest
                        canopy and gentle hills of the park, tea estate plantations and terraces
                        from neighbouring community gardens.</p>

                    <p>Cottages have been built to fit the local environment with a mixture of
                        Rwandese traditional art work and the Modern interior design and
                        furnishing rendering visitors to comfortably enjoying the beauty of the
                        area.</p>

                    <p>The design of the main building has been inspired by the Rwandese
                        traditional circular housing type rendering the balcony, restaurant and bar
                        suitable to enjoy 360o panoramic view of the area.</p>

                    <p>Similarly every cottage has been constructed with a particular view which
                        the visitor enjoys while sited at the balcony. Thus every cottage has been
                        named after a particular corresponding view. For instance when one
                        wants to enjoy the view and breeze from Lake Kivu, the cottage named
                        Kivu will be the choice whereas cottage named Nyungwe enables visitor to
                        enjoy the view of Nyungwe forest.</p>
                    <p>Also this site enables visitors to enjoy both the sunrise and the sunset. For
                        those who enjoy sunrise the most, the Sunrise wing cottages would be
                        their choice whereas those who enjoy the sunset, the choice would be the
                        sunset wing cottages. The balcony in the main building also offers an
                        opportunity to enjoy either the sunrise or sunset.</p>
                </div>
                <div class="col-md-10 col-md-push-1 text-center wow fadeInUp">
                    <h1 class="heading2">Structure and Designs</h1>
                </div>
                <div class="col-md-12 text-left text-center-sm  wow fadeInUp">
                    <p>The circular structure of the main building, the use of local material of the
                        cottages and a well mixture of the high quality fittings and furniture with
                        traditional settings is not only an attraction but also an added value that
                        visitors admire</p>
                </div>

                <div class="col-md-12">
                    <div class="rc-slider full-width">

                            <div class="slide-rc full-width">
                                <img class="hero-image" src="cmsImages/DSC04710-2.jpg" alt="">
                            </div>
                        <div class="slide-rc full-width">
                            <img class="hero-image" src="cmsImages/DJI_0031.jpg" alt="">
                        </div>
                        {{--<div class="slide-rc full-width">--}}
                            {{--<img class="hero-image" src="cmsImages/nyungwe3.png" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="slide-rc full-width">--}}
                            {{--<img class="hero-image" src="cmsImages/nyungwe4.png" alt="">--}}
                        {{--</div>--}}

                    </div>
                </div>

            </div>
        </div>
    </div>

<style>@media (max-width: 767px) {
        .slide-item h2 {
            display: none;
        }
    }
</style>
@include('layouts.footer')



<!--[if IE]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="files/javascripts/plugins/plugins.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.superslides.min.js"></script>
<script type="text/javascript" src="files/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="files/javascripts/owl.carousel.min.js"></script>

<script type="text/javascript" src="files/javascripts/wow.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.heapbox.min.js"></script>
<!--Searchbox JS-->
<script type="text/javascript" src="https://secure.mespilhotel.com/files/js/searchbox.js"></script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyAdJTGnKU-sV1N04pZPCXBHJQhbQsUzR8g" type="text/javascript" ></script>





<script type="text/javascript">
    var map;
    var clicked = null;
    //var center_point =  new google.maps.LatLng(48.6739848,1.7337709);




    var json = [
        {
            "id": 1,
            "title": "<strong>The Mespil Hotel</strong><br> Mespil Road, Dublin 4, Ireland.<br>Phone: +353 (0)1 488 4600<br>Email: mespil@leehotels.com",
            "latitude": "53.33332",
            "longitude": "-6.24672",
            "category": "hotel",
            "icon":"files/images/mapIcons/hotelnew.png"
        },
        {
            "id": 2,
            "title": "St. Stephen's Green",
            "latitude": "53.338164",
            "longitude": "-6.259090",
            "category": "landmark",
            "icon":"files/images/mapIcons/stephensgreen.png"
        }, {
            "id": 3,
            "title": "Grafton Street",
            "latitude": "53.341883",
            "longitude": "-6.260026",
            "category": "shop",
            "icon":"files/images/mapIcons/grafton.png"
        }, {
            "id": 4,
            "title": "Royal Dublin Society",
            "latitude": "53.325731",
            "longitude": "-6.229699",
            "category": "venues",
            "icon":"files/images/mapIcons/royal.png"
        }, {
            "id": 5,
            "title": "Aviva Stadium",
            "latitude": "53.335219",
            "longitude": "-6.228489",
            "category": "venues",
            "icon":"files/images/mapIcons/aviva.png"
        }, {
            "id": 6,
            "title": "Temple Bar",
            "latitude": "53.345469",
            "longitude": "-6.264218",
            "category": "landmark",
            "icon":"files/images/mapIcons/templebar.png"
        }, {
            "id": 7,
            "title": "Trinity College",
            "latitude": "53.343848",
            "longitude": "-6.254616",
            "category": "landmark",
            "icon":"files/images/mapIcons/trinity.png"
        }, {
            "id": 8,
            "title": "Ballsbridge",
            "latitude": "53.329227",
            "longitude": "-6.231388",
            "category": "landmark",
            "icon":"files/images/mapIcons/ballsbridge.png"
        }, {
            "id": 9,
            "title": "National Concert Hall",
            "latitude": "53.334761",
            "longitude": "-6.259209",
            "category": "venues",
            "icon":"files/images/mapIcons/concerthall.png"
        }, {
            "id": 10,
            "title": "Board Gais Energy Theatre",
            "latitude": "53.344020",
            "longitude": "-6.240094",
            "category": "venues",
            "icon":"files/images/mapIcons/bordgais.png"
        }, {
            "id": 11,
            "title": "The 3areana",
            "latitude": "53.347443",
            "longitude": "-6.228643",
            "category": "venues",
            "icon":"files/images/mapIcons/3arena.png"
        }, {
            "id": 12,
            "title": "Convention Centre Dublin",
            "latitude": "53.347980",
            "longitude": "-6.239559",
            "category": "venues",
            "icon":"files/images/mapIcons/convention.png"
        }, {
            "id": 13,
            "title": "Shelbourne Race Track",
            "latitude": "53.340451",
            "longitude": "-6.230610",
            "category": "venues",
            "icon":"files/images/mapIcons/shelbourne.png"
        },


    ];
    var markers = [];
    var infowindow = new google.maps.InfoWindow();
    var myLatLng = {lat: 53.33500, lng:-6.24672};

    function initialize() {

        var styles =
            [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#444444"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                }
            ]




        var styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"});

        var ww = $(window).width();

        if(ww > 768){
            var Drag = true;
        }else{
            var Drag = false;
        }

        // Giving the map som options
        var mapOptions = {
            animation: google.maps.Animation.DROP,
            draggable: Drag,
            zoom: 15,
            scrollwheel: false,
            mapTypeControl:false,
            clickableIcons: false,
            streetViewControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            center: myLatLng,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };
        // Creating the map
        map = new google.maps.Map(document.getElementById('map'), mapOptions);

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        directionsDisplay = new google.maps.DirectionsRenderer();
        directionsService = new google.maps.DirectionsService();
        directionsDisplay.setMap(map);
        directionsDisplay.setOptions({polylineOptions:{strokeColor:"#87b6ac",strokeWeight:5}, suppressMarkers:true })
        directionsDisplay.setPanel(document.getElementById("directions-load"));

        // Looping through all the entries from the JSON data
        for( var i = 0; i < json.length; i++ ) {

            // Current object
            var obj = json[i];
            var position =  new google.maps.LatLng(obj.latitude,obj.longitude);
            var marker_icon = obj.icon;
            var category1 = obj.category;
            //console.log(marker_icon);
            // Adding a new marker for the object
            //
            create_markers(obj,position,marker_icon,category1);


        } // end loop

        function create_markers(obj,position,marker_icon,category1){


            var marker = new google.maps.Marker({
                position: position,
                icon: marker_icon,
                category: category1,
                map: map,
                animation: google.maps.Animation.DROP,
                title: obj.title // this works, giving the marker a title with the correct title
            });

            markers.push(marker);



        }


        /*google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });*/

        /**
         * Function to filter markers by category
         */

        filterMarkers = function (category1) {
            markers[0].setVisible(true);
            if(clicked == category1){
                for (i = 1; i < json.length; i++) {
                    marker = markers[i];
                    marker.setVisible(true);
                    clicked = null;
                }
            }else{
                clicked = category1;
                for (i = 1; i < json.length; i++) {
                    marker = markers[i];
                    // If is same category or category not picked
                    if (marker.category == category1 || category1.length === 0) {
                        marker.setVisible(true);
                    }
                    // Categories don't match
                    else {
                        marker.setVisible(false);
                    }
                }
            }
        }



        // Adding a new click event listener for the object
        function addClicker(marker, content, infowindow) {

            google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {
                return function(){
                    infowindow.setContent(content);
                    infowindow.open(map,marker);
                };
            })(marker, content, infowindow));

        }

        for (var i = 0; i < Object.keys(markers).length; i++){
            //console.log(i, markers[i]);
            google.maps.event.trigger(markers[i], 'click');
        }
// google.maps.event.trigger(markers[1], 'click');

    }//

    function calcRoute() {
        var start = document.getElementById('dirFrom').value;
        var end = myLatLng;
        var request = {
            origin: start,
            destination: end,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function(response, status){
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            } else {
                alert("We could not find directions for your request, please check your From and To inputs then try again.");
            };
        });
        $('#directions-load').addClass('active');
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    $(function() {

        $(".directions #dirFrom").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $('.directions .submit-button').click();
            }
        });

    });



</script>


<script type="text/javascript" src="files/javascripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="files/javascripts/custom.js"></script>

<!--  -->


</body>
</html>


@endsection



