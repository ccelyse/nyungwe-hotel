
<!DOCTYPE html>
<html>
<head>
    <title>Contact Us - Get In Touch | Mespil Hotel Dublin</title>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-decetion" content="telephone=no"/>
    <meta name="keywords" content="" />
    <meta name="description" content="Please get in touch if you have any queries about accommodation, meetings or dining at The Mespil Hotel Dublin. We will respond as soon as possible to your email." />

    <!-- links -->
    <link rel="shortcut icon" type="image/x-icon" href="files/images/icon.png" >
    <link href="files/stylesheets/styles.css" type="text/css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if !IE]>
    <!-->

    <script>
        if (/*@cc_on!@*/false) {document.documentElement.className+='ie10';}
    </script>

    <!--<![endif]-->

    <!--[if IE]>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <![endif]-->
    <!--[if !IE]><!-->
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!--<![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>


    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5JNLJG');</script>
    <!-- End Google Tag Manager -->

</head><body class="color default" style="opacity:0;">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5JNLJG"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--  -->

<header class="header wow fadeInDown" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">
                <div class="logo">
                    <a href="index.html"><img src="files/images/logo.png" alt="logo" /></a>
                    <!-- BEGIN OF SWING TAG -->


                    <style>.gift-tag-swing{
                            display: inline-block;
                            position:absolute;
                            top: 100px;
                            left: 50%;
                            margin-left: -86px;
                            z-index: -2;
                            background-color:transparent !important
                        }
                        header.fixed .gift-tag-swing{
                            opacity: 0;
                            visibility: hidden;
                        }

                        @media (max-width: 767px){
                            .gift-tag-swing{
                                display:none !important;
                            }

                        }
                        @media (min-width: 768px){
                            .gift-tag-swing{
                                top: 82px;
                                right: 50px;
                                left: auto;
                            }

                        }
                        @media (min-width: 992px){
                            .gift-tag-swing{
                                top: 74px;
                                left: 42px;
                                margin-left: 0;
                            }
                            header.fixed .gift-tag-swing{
                                top: 32px;
                                left: 42px;
                                margin-left: 0;
                            }
                        }
                        @media (min-width: 1200px){
                            .gift-tag-swing{
                                top:94px;
                                left: 4px;
                                margin-left:-28px;
                            }
                            header.fixed .gift-tag-swing{
                                top: 31px;
                                left: 10px;
                                margin-left: 0;
                            }
                        }
                        @-webkit-keyframes swingimage{
                            0%{-webkit-transform:rotate(10deg)}
                            50%{-webkit-transform:rotate(-5deg)}
                            100%{-webkit-transform:rotate(10deg)}
                        }
                        @keyframes swinging{
                            0%{transform:rotate(10deg)}
                            50%{transform:rotate(-5deg)}
                            100%{transform:rotate(10deg)}
                        }
                        .swingimage{
                            -webkit-transform-origin:50% 0;
                            transform-origin:50% 0;
                            -webkit-animation:swinging 3.5s ease-in-out forwards infinite;
                            animation:swinging 3.5s ease-in-out forwards infinite
                        }
                    </style>



                    <!--<a class="gift-tag-swing" href="https://secure.mespilhotel.com/bookings/specials/winter-sale">
                        <img class="swingimage" src="/files/images/swinging-tag.png" alt="swing tag voucher image" />
                    </a>-->

                    <!-- END OF SWING TAG -->                </div>
                <div class="headerTop hidden-sm hidden-xs">
                    <span class="phone"><i class="zmdi zmdi-hc-fw zmdi-phone"></i> +353 1 488 4600</span>
                    <div class="clearfix"></div>
                    <nav class="mainMenu">

                        <ul>
                            <li class="first"><a href="rooms.html">Rooms</a>
                                <ul>
                                    <li class="first"><a href="superior.htm">Superior Executive</a></li>
                                    <li><a href="signature-room.htm">Signature Rooms</a></li>
                                    <li><a href="executive-room.html">Executive Rooms</a></li>
                                    <li><a href="family-room.html">Family Room</a></li>
                                    <li class="last"><a href="fitness-suite.html">Fitness Suite</a></li>
                                </ul>
                            </li>
                            <li><a href="https://secure.mespilhotel.com/bookings/specialspage/">Offers</a></li>
                            <li><a href="location.html">Location</a>
                                <ul>
                                    <li class="first"><a href="directions.html">Directions</a></li>
                                    <li><a href="brochure.htm">Brochure</a></li>
                                    <li class="last"><a href="discover-dublin.html">Discover Dublin</a>
                                        <ul>
                                            <li class="first"><a href="getting-around.html">Getting around </a></li>
                                            <li><a href="dublin-tourist-attractions.html">Attractions</a></li>
                                            <li><a href="family-activities.html">Family Activities</a></li>
                                            <li><a href="local-area.html">Local Area </a></li>
                                            <li class="last"><a href="https://www.mespilhotel.com/dublin-city-events.html">Dublin Events </a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="dining.html">Dining</a>
                                <ul>
                                    <li class="first"><a href="lock-four.html">Lock Four</a></li>
                                    <li class="last"><a href="the-lounge.html">The Lounge</a></li>
                                </ul>
                            </li>
                            <li><a href="business.html">Business</a>
                                <ul>
                                    <li class="first"><a href="mespil-boardrooms.html">Boardrooms</a></li>
                                    <li><a href="corporate-loyalty-club.htm">Loyalty Club</a></li>
                                    <li><a href="business-fitness.htm">Fitness Suite</a></li>
                                    <li class="last"><a href="businesscenter.htm">Business Center</a></li>
                                </ul>
                            </li>
                            <li><a href="gallery.html">Gallery</a></li>
                            <li class="active"><a href="contact-us.html">Contact</a>
                                <ul>
                                    <li class="first"><a href="careers.htm">Careers</a></li>
                                </ul>
                            </li>
                            <li class="last"><a href="https://secure.mespilhotel.com/bookings/vouchers/">Vouchers</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="book-box hidden-sm hidden-xs">
                    <form method="post" action="https://secure.mespilhotel.com/bookings/checkavailability" id="bke_searchbox_data" style="display:none;" >
                        <input type="hidden" value="1" id="bke_minstay">
                        <input type="hidden" value="364" id="bke_maxstay">
                        <input type="hidden" name="bke_form_token" value="" id="bke_form_token">
                        <input type="hidden" name="grid_view" value="2" id="bke_grid_view">
                        <input type="hidden" name="lang" value="en" id="bke_lang">
                        <input type="hidden" name="bke_ratecode" value="" id="bke_ratecode">
                        <input type="hidden" name="bke_arrival_day" value="31" id="bke_arrival_day">
                        <input type="hidden" name="bke_arrival_month" value="10" id="bke_arrival_month">
                        <input type="hidden" name="bke_arrival_year" value="2018" id="bke_arrival_year">
                        <input type="hidden" name="bke_departure_day" value="" id="bke_departure_day">
                        <input type="hidden" name="bke_departure_month" value="" id="bke_departure_month">
                        <input type="hidden" name="bke_departure_year" value="" id="bke_departure_year">
                        <input type="hidden" name="bke_nights" value="1" id="bke_nights">

                    </form>
                    <div class="na-searchbox">
                        <div class="na-searchbox__parameters">
                            <input type="hidden" name="searchbox_type" value="">
                            <input type="hidden" name="searchbox_display" value="boxes">
                            <input type="hidden" name="searchbox_size" value="">
                            <input type="hidden" name="searchbox_flexdate" value="">
                            <input type="hidden" name="searchbox_usp" value="">
                            <input type="hidden" name="searchbox_collapse" value="yes">
                            <input type="hidden" name="searchbox_ratecode" value="">
                            <input type="hidden" name="searchbox_hidden" value="no">
                            <input type="hidden" name="searchbox_debug" value="">
                            <input type="hidden" name="searchbox_location" value="">
                        </div>
                        <div class="na-searchbox__header">
                            Book Your Stay
                        </div>
                        <div class="na-searchbox__main">

                            <div class="na-searchbox__dates">
                                <div class="na-searchbox__checkin">
                                    <span class="na-searchbox__label">Check in</span>
                                    <div class="na-searchbox__day-week"></div>
                                    <div class="na-searchbox__day-number">31</div>
                                    <div class="na-searchbox__month">10</div>
                                    <div class="na-searchbox__year">2018</div>
                                </div>
                            </div>
                            <!--if bke_ratecode_section-->
                            <div class="na-searchbox__ratecode">
                                <input type="text" class="na-searchbox__ratecode-input" name="ratecode" placeholder="Have a promo code?">
                            </div>
                            <!--endif bke_ratecode_section-->
                            <button class="na-searchbox__submit">Search</button>
                        </div>
                        <!-- IF bke_usp -->
                        <div class="na-searchbox__usptrigger">
                            Why Book With Us? <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Mespil Hotel">
                        </div>
                        <!-- ENDIF bke_usp -->
                        <!-- IF bke_usp -->
                        <div class="na-searchbox__uspoverlay">
                            <div class="na-searchbox__uspcontainer">
                                <div class="na-searchbox__uspclose"><i class="zmdi zmdi-close-circle-o"></i></div>
                                <div class="na-searchbox__uspheading">Why Book With Us? <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Mespil Hotel"></div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Best Price Guarantee">
                                    <span class="na-searchbox__usptitle">Best Price Guarantee</span>
                                    <span class="na-searchbox__usptext">Best rate available online!</span>
                                </div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="No Booking Fee">
                                    <span class="na-searchbox__usptitle">No Booking Fee</span>
                                    <span class="na-searchbox__usptext">Book now, pay later.</span>
                                </div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Free Wi-Fi">
                                    <span class="na-searchbox__usptitle">Free Wi-Fi</span>
                                    <span class="na-searchbox__usptext">500mb High Speed Wi-Fi available throughout the hotel!</span>
                                </div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Free Cancellation">
                                    <span class="na-searchbox__usptitle">Free Cancellation</span>
                                    <span class="na-searchbox__usptext">Up to 24 hours prior</span>
                                </div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Fitness Suite">
                                    <span class="na-searchbox__usptitle">Fitness Suite</span>
                                    <span class="na-searchbox__usptext">Complimentary access! (18+)</span>
                                </div>

                            </div>
                        </div>
                        <!-- ENDIF bke_usp -->
                        <div class="na-searchbox__overlay">
                            <div class="na-searchbox__overlaylogo">
                                <div class="na-searchbox__overlaylogowrapper">
                                    <img src="https://secure.mespilhotel.com/bookings/showimage?type=18&amp;id=21B785F10889100848E2485F66B4B250" alt="Mespil Hotel"/>
                                </div>
                                <div class="na-searchbox__overlayclose"><i class="zmdi zmdi-close-circle-o"></i></div>
                            </div>
                            <div class="na-searchbox__overlaywrapper">
                                <div class="na-searchbox__overlayscroll">
                                    <div class="na-searchbox__header">
                                        Book Your Stay
                                    </div>
                                    <!-- IF bke_usp -->
                                    <div class="na-searchbox__overlayusp">
                                        <div class="na-searchbox__overlayuspwrapper">
                                            <div class="na-searchbox__overlayuspheading">Why Book With Us?</div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Best Price Guarantee">
                                                <span class="na-searchbox__overlayusptitle">Best Price Guarantee</span>
                                                <span class="na-searchbox__overlayusptext">Best rate available online!</span>
                                            </div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="No Booking Fee">
                                                <span class="na-searchbox__overlayusptitle">No Booking Fee</span>
                                                <span class="na-searchbox__overlayusptext">Book now, pay later.</span>
                                            </div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Free Wi-Fi">
                                                <span class="na-searchbox__overlayusptitle">Free Wi-Fi</span>
                                                <span class="na-searchbox__overlayusptext">500mb High Speed Wi-Fi available throughout the hotel!</span>
                                            </div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Free Cancellation">
                                                <span class="na-searchbox__overlayusptitle">Free Cancellation</span>
                                                <span class="na-searchbox__overlayusptext">Up to 24 hours prior</span>
                                            </div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Fitness Suite">
                                                <span class="na-searchbox__overlayusptitle">Fitness Suite</span>
                                                <span class="na-searchbox__overlayusptext">Complimentary access! (18+)</span>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- ENDIF bke_usp -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile-element pull-right visible-sm visible-xs">
                    <div class="mb-nav-btn">
                        <img src="files/images/menu.png" alt="">
                    </div>
                    <a href="tel:+35314884600">
                        <div class="mb-ph">
                            <i class="zmdi zmdi-hc-fw zmdi-phone"></i> <br>
                            <span>CALL</span>
                            <div class="ph-hd"> +353 1 488 4600</div>
                        </div>
                    </a>
                    <div class="book-mb">
                        <div class="book-hd open-searchbox-02">
                            <span class="hidden-sm hidden-xs">Book Now</span>
                            <span class="hidden-md hidden-lg">Book Now</span>
                            <i class="fa fa-calendar-check-o"></i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>
<nav class="mobile-nav">
    <img class="closeMenu" src="files/images/closeMenu.png" alt="">
    <div class="container">
        <ul>
            <li class="visible-sm visible-xs"><a href="index.html"><img src="files/images/logo.png" alt="logo" /></a> </li>
        </ul>

        <ul>
            <li class="first"><a href="rooms.html">Rooms</a>
                <ul>
                    <li class="first"><a href="superior.htm">Superior Executive</a></li>
                    <li><a href="signature-room.htm">Signature Rooms</a></li>
                    <li><a href="executive-room.html">Executive Rooms</a></li>
                    <li><a href="family-room.html">Family Room</a></li>
                    <li class="last"><a href="fitness-suite.html">Fitness Suite</a></li>
                </ul>
            </li>
            <li><a href="https://secure.mespilhotel.com/bookings/specialspage/">Offers</a></li>
            <li><a href="location.html">Location</a>
                <ul>
                    <li class="first"><a href="directions.html">Directions</a></li>
                    <li><a href="brochure.htm">Brochure</a></li>
                    <li class="last"><a href="discover-dublin.html">Discover Dublin</a>
                        <ul>
                            <li class="first"><a href="getting-around.html">Getting around </a></li>
                            <li><a href="dublin-tourist-attractions.html">Attractions</a></li>
                            <li><a href="family-activities.html">Family Activities</a></li>
                            <li><a href="local-area.html">Local Area </a></li>
                            <li class="last"><a href="https://www.mespilhotel.com/dublin-city-events.html">Dublin Events </a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="dining.html">Dining</a>
                <ul>
                    <li class="first"><a href="lock-four.html">Lock Four</a></li>
                    <li class="last"><a href="the-lounge.html">The Lounge</a></li>
                </ul>
            </li>
            <li><a href="business.html">Business</a>
                <ul>
                    <li class="first"><a href="mespil-boardrooms.html">Boardrooms</a></li>
                    <li><a href="corporate-loyalty-club.htm">Loyalty Club</a></li>
                    <li><a href="business-fitness.htm">Fitness Suite</a></li>
                    <li class="last"><a href="businesscenter.htm">Business Center</a></li>
                </ul>
            </li>
            <li><a href="gallery.html">Gallery</a></li>
            <li class="active"><a href="contact-us.html">Contact</a>
                <ul>
                    <li class="first"><a href="careers.htm">Careers</a></li>
                </ul>
            </li>
            <li class="last"><a href="https://secure.mespilhotel.com/bookings/vouchers/">Vouchers</a></li>
        </ul>
    </div>
</nav>

<form method="post" action="https://secure.mespilhotel.com/bookings/checkavailability" id="bke_searchbox_data" style="display:none;" >
    <input type="hidden" value="1" id="bke_minstay">
    <input type="hidden" value="364" id="bke_maxstay">
    <input type="hidden" name="bke_form_token" value="" id="bke_form_token">
    <input type="hidden" name="grid_view" value="2" id="bke_grid_view">
    <input type="hidden" name="lang" value="en" id="bke_lang">
    <input type="hidden" name="bke_ratecode" value="" id="bke_ratecode">
    <input type="hidden" name="bke_arrival_day" value="31" id="bke_arrival_day">
    <input type="hidden" name="bke_arrival_month" value="10" id="bke_arrival_month">
    <input type="hidden" name="bke_arrival_year" value="2018" id="bke_arrival_year">
    <input type="hidden" name="bke_departure_day" value="" id="bke_departure_day">
    <input type="hidden" name="bke_departure_month" value="" id="bke_departure_month">
    <input type="hidden" name="bke_departure_year" value="" id="bke_departure_year">
    <input type="hidden" name="bke_nights" value="1" id="bke_nights">

</form>
<div class="na-searchbox">
    <div class="na-searchbox__parameters">
        <input type="hidden" name="searchbox_type" value="">
        <input type="hidden" name="searchbox_display" value="overlay">
        <input type="hidden" name="searchbox_size" value="">
        <input type="hidden" name="searchbox_flexdate" value="">
        <input type="hidden" name="searchbox_usp" value="">
        <input type="hidden" name="searchbox_collapse" value="">
        <input type="hidden" name="searchbox_ratecode" value="">
        <input type="hidden" name="searchbox_hidden" value="yes">
        <input type="hidden" name="searchbox_debug" value="">
        <input type="hidden" name="searchbox_location" value="">
    </div>
    <div class="na-searchbox__header">
        Book Your Stay
    </div>
    <div class="na-searchbox__main">

        <div class="na-searchbox__dates">
            <div class="na-searchbox__checkin">
                <span class="na-searchbox__label">Check in</span>
                <div class="na-searchbox__day-week"></div>
                <div class="na-searchbox__day-number">31</div>
                <div class="na-searchbox__month">10</div>
                <div class="na-searchbox__year">2018</div>
            </div>
        </div>
        <!--if bke_ratecode_section-->
        <div class="na-searchbox__ratecode">
            <input type="text" class="na-searchbox__ratecode-input" name="ratecode" placeholder="Have a promo code?">
        </div>
        <!--endif bke_ratecode_section-->
        <button class="na-searchbox__submit">Search</button>
    </div>
    <!-- IF bke_usp -->
    <div class="na-searchbox__usptrigger">
        Why Book With Us? <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Mespil Hotel">
    </div>
    <!-- ENDIF bke_usp -->
    <!-- IF bke_usp -->
    <div class="na-searchbox__uspoverlay">
        <div class="na-searchbox__uspcontainer">
            <div class="na-searchbox__uspclose"><i class="zmdi zmdi-close-circle-o"></i></div>
            <div class="na-searchbox__uspheading">Why Book With Us? <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Mespil Hotel"></div>

            <div class="na-searchbox__usplistitem">
                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Best Price Guarantee">
                <span class="na-searchbox__usptitle">Best Price Guarantee</span>
                <span class="na-searchbox__usptext">Best rate available online!</span>
            </div>

            <div class="na-searchbox__usplistitem">
                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="No Booking Fee">
                <span class="na-searchbox__usptitle">No Booking Fee</span>
                <span class="na-searchbox__usptext">Book now, pay later.</span>
            </div>

            <div class="na-searchbox__usplistitem">
                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Free Wi-Fi">
                <span class="na-searchbox__usptitle">Free Wi-Fi</span>
                <span class="na-searchbox__usptext">500mb High Speed Wi-Fi available throughout the hotel!</span>
            </div>

            <div class="na-searchbox__usplistitem">
                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Free Cancellation">
                <span class="na-searchbox__usptitle">Free Cancellation</span>
                <span class="na-searchbox__usptext">Up to 24 hours prior</span>
            </div>

            <div class="na-searchbox__usplistitem">
                <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Fitness Suite">
                <span class="na-searchbox__usptitle">Fitness Suite</span>
                <span class="na-searchbox__usptext">Complimentary access! (18+)</span>
            </div>

        </div>
    </div>
    <!-- ENDIF bke_usp -->
    <div class="na-searchbox__overlay">
        <div class="na-searchbox__overlaylogo">
            <div class="na-searchbox__overlaylogowrapper">
                <img src="https://secure.mespilhotel.com/bookings/showimage?id=21B785F10889100848E2485F66B4B250&amp;type=18" alt="Mespil Hotel"/>
            </div>
            <div class="na-searchbox__overlayclose"><i class="zmdi zmdi-close-circle-o"></i></div>
        </div>
        <div class="na-searchbox__overlaywrapper">
            <div class="na-searchbox__overlayscroll">
                <div class="na-searchbox__header">
                    Book Your Stay
                </div>
                <!-- IF bke_usp -->
                <div class="na-searchbox__overlayusp">
                    <div class="na-searchbox__overlayuspwrapper">
                        <div class="na-searchbox__overlayuspheading">Why Book With Us?</div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Best Price Guarantee">
                            <span class="na-searchbox__overlayusptitle">Best Price Guarantee</span>
                            <span class="na-searchbox__overlayusptext">Best rate available online!</span>
                        </div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="No Booking Fee">
                            <span class="na-searchbox__overlayusptitle">No Booking Fee</span>
                            <span class="na-searchbox__overlayusptext">Book now, pay later.</span>
                        </div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Free Wi-Fi">
                            <span class="na-searchbox__overlayusptitle">Free Wi-Fi</span>
                            <span class="na-searchbox__overlayusptext">500mb High Speed Wi-Fi available throughout the hotel!</span>
                        </div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Free Cancellation">
                            <span class="na-searchbox__overlayusptitle">Free Cancellation</span>
                            <span class="na-searchbox__overlayusptext">Up to 24 hours prior</span>
                        </div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="https://secure.mespilhotel.com/files/images/usp-tick.svg" alt="Fitness Suite">
                            <span class="na-searchbox__overlayusptitle">Fitness Suite</span>
                            <span class="na-searchbox__overlayusptext">Complimentary access! (18+)</span>
                        </div>

                    </div>
                </div>
                <!-- ENDIF bke_usp -->
            </div>
        </div>
    </div>
</div>

<!--Header End-->

<!-- 7854 -->

<div class="slider-height" style="background-color: #000;">

    <div id="slides" class="home-slider superslides full-width">
        <div class="slides-container">

            <div class="slide-item">
                <img class="hero-image" src="cmsGallery/imagerow/7854/resized/1600x693/mespil_hotel_dublin_lobby_long.jpg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>Contact Us</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item">
                <img class="hero-image" src="cmsGallery/imagerow/7854/resized/1600x693/superior_room_banner.png" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>Contact Us</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item">
                <img class="hero-image" src="cmsGallery/imagerow/7854/resized/1600x693/business_center_1600_x_693.jpg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>Contact Us</h2>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav class="slides-navigation">
            <a href="contact-us.html#" class="next"></a>
            <a href="contact-us.html#" class="prev"></a>
        </nav>
        <a class="nextblock wow fadeInDown"  data-wow-delay="0.5s" href="contact-us.html#Next-block">
            <div class="verticalCenter">
                <div class="verticalInner">
                    <span>Scroll</span>
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>
        </a>
    </div>
    <!--Slider End-->

</div>

<section id="Next-block" class="callout font-15 full-width glaze-bistro">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Contact Us</h1>
            </div><!--col-->
            <div class="col-sm-12 col-md-10 col-md-offset-1 text-left wow fadeInDown">
                <div><strong>Postal address:</strong>&nbsp;Mespil Hotel, 50 &ndash; 60 Mespil Road, Dublin 4, D04 E7N2, Ireland<br /><strong>Phone:</strong>&nbsp;+353 1 488 4600<br /><strong>Fax:</strong>&nbsp;+353 1 667 1244<br /><strong>Email:</strong>&nbsp;<a href="mailto:mespil@leehotels.com" target="_blank">mespil@leehotels.com</a></div>
                <div>&nbsp;</div>
                <div><div class="form-builder-wrapper"><noscript>
                            <p class="frm-bldr-error">JavaScript must be enabled for this form to work</p></noscript>
                        <link type="text/css" property="" rel="stylesheet" href="global-css/form-builder.css"><link type="text/css" property="" rel="stylesheet" href="global-css/jquery-ui.css"><script type="text/javascript" src="global-js/jquery.validate.min.js"></script>
                        <script type="text/javascript" src="global-js/placeholders.min.js"></script>
                        <script type="text/javascript" src="global-js/form-builder-init.js"></script>
                        <form class="frm-bldr" enctype="multipart/form-data" method="post" action="/forms2/submit/21930D518E7B6AA318148EFFDC71756A">
                            <input type="hidden" name="data[Forms2][system][form_url]" value="/contact-us.html?devicetype=pc" /><ul>
                                <li class="input_text first odd" id="fld-name">
                                    <div class="label-wrapper"><label for="name">Name<span class="required">*</span></label>
                                    </div><div class="field-wrapper"><input type="text"
                                                                            id="name"
                                                                            name="data[Forms2][name]"
                                                                            value=""
                                                                            placeholder=""
                                                                            class="loosetext required" />
                                    </div><div class="clear"></div></li><li class="input_text even" id="fld-email">
                                    <div class="label-wrapper"><label for="email">Email<span class="required">*</span></label>
                                    </div><div class="field-wrapper"><input type="text"
                                                                            id="email"
                                                                            name="data[Forms2][email]"
                                                                            value=""
                                                                            placeholder=""
                                                                            class="email required" />
                                    </div><div class="clear"></div></li><li class="input_text odd" id="fld-telephone">
                                    <div class="label-wrapper"><label for="telephone">Telephone</label>
                                    </div><div class="field-wrapper"><input type="text"
                                                                            id="telephone"
                                                                            name="data[Forms2][telephone]"
                                                                            value=""
                                                                            placeholder=""
                                                                            class="phone" />
                                    </div><div class="clear"></div></li><li class="textarea even" id="fld-message">
                                    <div class="label-wrapper"><label for="message">Message<span class="required">*</span></label>
                                    </div><div class="field-wrapper"><textarea id="message"
                                                                               name="data[Forms2][message]"
                                                                               class="loosetext required"></textarea>
                                    </div><div class="clear"></div></li><li class="radio_yes_no last odd" id="fld-newsletter_sign_up">
                                    <div class="label-wrapper"><label for="newsletter_sign_up">Newsletter Sign Up<span class="required">*</span></label>
                                    </div><div class="field-wrapper"><span class="multi-row">
<span class="row clearfix"><input type="radio"
                                  id="newsletter_sign_up-yes"
                                  name="data[Forms2][newsletter_sign_up]"
                                  value="Yes" checked="checked" autocomplete="off" /><label for="newsletter_sign_up-yes">Yes</label></span>
<span class="row clearfix"><input type="radio"
                                  id="newsletter_sign_up-no"
                                  name="data[Forms2][newsletter_sign_up]"
                                  value="No" autocomplete="off" /><label for="newsletter_sign_up-no">No</label></span>
</span>
                                        <input type="hidden"
                                               id="newsletter_sign_up_reqval"
                                               name="data[Forms2][system][newsletter_sign_up_reqval]"
                                               class="required_radio" /></div><div class="clear"></div></li><li class="btn-submit"><input type="submit" name="data[Forms2][system][submit]"
                                                                                                                                          value="Send" disabled="disabled" /></li>
                            </ul>
                        </form>
                    </div></div>
            </div>
            <div class="col-md-12 menu-button-room wow fadeInDown">
            </div><!--col-->
        </div><!--row-->
    </div><!--container-->
</section>
<!--call out end-->

<!--  -->



<!--  -->


<section class="location_map">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="location-content wow fadeInRight">
            <div class="box">
                <h3>MESPIL HOTEL<span>GALLERY</span></h3>
                <a href="gallery.html">view</a>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="location-content2 wow fadeInLeft">
            <div class="box2">
                <h3>MESPIL HOTEL<span>Location</span></h3>
                <a href="location.html">view</a>
            </div>
        </div>
    </div>
</section>
<section class="news-letter">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4><span class="fa fa-envelope fa-lg"></span>SIGN UP FOR SPECIAL OFFERS</h4>
                <div class="form-div"><div class="form-builder-wrapper"><noscript>
                            <p class="frm-bldr-error">JavaScript must be enabled for this form to work</p></noscript>
                        <link type="text/css" property="" rel="stylesheet" href="global-css/form-builder.css"><link type="text/css" property="" rel="stylesheet" href="global-css/jquery-ui.css"><script type="text/javascript" src="global-js/jquery.validate.min.js"></script>
                        <script type="text/javascript" src="global-js/placeholders.min.js"></script>
                        <script type="text/javascript" src="global-js/form-builder-init.js"></script>
                        <form class="frm-bldr" enctype="multipart/form-data" method="post" action="/forms2/submit/BD404A0CB9F7DC08FC4854D04B0941DA">
                            <input type="hidden" name="data[Forms2][system][form_url]" value="/newsletter.html" /><ul>
                                <li class="input_text first odd" id="fld-email">
                                    <div class="label-wrapper"><label for="email">Email<span class="required">*</span></label>
                                    </div><div class="field-wrapper"><input type="text"
                                                                            id="email"
                                                                            name="data[Forms2][email]"
                                                                            value=""
                                                                            placeholder="your@email.com"
                                                                            class="email required" />
                                    </div><div class="clear"></div></li><li class="checkbox even" id="fld-consent">
                                    <div class="label-wrapper"><label for="consent">&nbsp;</label>
                                    </div><div class="field-wrapper"><span class="multi-row clearfix">
<span class="row clearfix"><input  required  type="checkbox" id="consent-i_would_like_to_receive_emails_with_special_offers" name="data[Forms2][consent][]" value="I would like to receive emails with special offers" /><label for="consent-i_would_like_to_receive_emails_with_special_offers">I would like to receive emails with special offers</label></span>
</span>
                                        <input type="hidden"
                                               id="consent_reqval"
                                               name="data[Forms2][system][consent_reqval]"
                                               class="required_checkbox" /></div><div class="clear"></div></li><input type="hidden" id="mailchimp-7881b376fc" name="data[Forms2][mailchimp][7881b376fc]" value="mailchimp"Mespil Hotel Special Offers /><li class="btn-submit"><input type="submit" name="data[Forms2][system][submit]"
                                                                                                                                                                                                                                                                                      value="Sign Up" disabled="disabled" /></li>
                            </ul>
                        </form>
                    </div></div>
            </div>
        </div>
    </div>
</section>

<!--  -->
<footer class="full-width">
    <div class="footer-nav hidden-lg hidden-md text-center">
        <img src="files/images/menu.png" alt="" class="footer-menu-t">
        <nav class="full-width footer-menu display-none">

            <ul>
                <li class="first"><a href="loyalty-club.html">Loyalty Club</a></li>
                <li><a href="https://secure.mespilhotel.com/bookings/vouchers/">Vouchers</a></li>
                <li><a href="https://secure.mespilhotel.com/bookings/reviews/">Reviews</a></li>
                <li><a href="sitemap.html">Sitemap</a></li>
                <li><a href="privacy-policy.html">Privacy Policy</a></li>
                <li><a href="discover-dublin.html">Discover Dublin</a></li>
                <li><a href="cmsFiles/email_web_mespil_a5_brochure.pdf" target="_blank">Brochure</a></li>
                <li class="last"><a href="javascript: Cookiebot.renew()">Cookie Notice</a></li>
            </ul>
        </nav>
    </div>
    <div class="footerTop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="specialOffer visible-lg-inline-block"><a href="sign-up.html">Sign up for Special Offers</a></p>
                    <nav class="footerMenu hidden-sm hidden-xs">

                        <ul>
                            <li class="first"><a href="loyalty-club.html">Loyalty Club</a></li>
                            <li><a href="https://secure.mespilhotel.com/bookings/vouchers/">Vouchers</a></li>
                            <li><a href="https://secure.mespilhotel.com/bookings/reviews/">Reviews</a></li>
                            <li><a href="sitemap.html">Sitemap</a></li>
                            <li><a href="privacy-policy.html">Privacy Policy</a></li>
                            <li><a href="discover-dublin.html">Discover Dublin</a></li>
                            <li><a href="cmsFiles/email_web_mespil_a5_brochure.pdf" target="_blank">Brochure</a></li>
                            <li class="last"><a href="javascript: Cookiebot.renew()">Cookie Notice</a></li>
                        </ul>
                    </nav>
                    <div class="social">
                        <span>FOLLOW US</span>
                        <ul>
                            <li class="fb"><a href="https://www.facebook.com/pages/Dublin-Ireland/Mespil-Hotel/61218316670" target="_blank"></a></li>
                            <li class="twitter"><a href="https://twitter.com/MespilHotel" target="_blank"></a></li>
                            <!--<!--<li class="google"><a href="https://plus.google.com/108877475206796989243/posts" target="_blank"></a></li>-->
                            <li class="instagram"><a href="https://www.instagram.com/mespilhoteldublin/" target="_blank"></a></li>
                        </ul>
                    </div>
                    <p class="copyright hidden-lg hidden-md">©2018 The Mespil Hotel, 50-60 Mespil Road, Dublin 4, Ireland  D04 E7N2</p>
                    <div class="row hidden-lg hidden-md">
                        <div class="footer-mb-ph col-sm-6 col-xs-12 text-right">Phone : +353 1 488 4600</div>
                        <div class="footer-mb-em col-sm-6 col-xs-12 text-left">Email : <a href="mailto:mespil@leehotels.com">mespil@leehotels.com</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--footerTop-->
    <div class="footerBottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyfix">
                        <p class="copyright hidden-sm hidden-xs">©2018 The Mespil Hotel, 50-60 Mespil Road, Dublin 4, Ireland  D04 E7N2</p>
                        <ul class="hidden-sm hidden-xs">
                            <li>Phone : +353 1 488 4600</li>
                            <li>Email : <a href="mailto:mespil@leehotels.com">mespil@leehotels.com</a></li>
                        </ul>
                    </div>
                    <div class="logo2">

                        <!-- GOOGLE LANGUAGES -->
                        <div id="google_translate_element"></div>
                        <!--
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                              new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                            }
                        </script>
                         -->
                        <!-- <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> -->
                        <!-- GOOGLE LANGUAGES -->

                        <a href="https://www.sligoparkhotel.com/" target="_blank"><img src="files/images/logo3.png" alt="Lee Sligo Park" /></a>
                        <a href="https://www.leehotels.com/" target="_blank"><img src="files/images/logo2.png" alt="Lee Hotels" /></a>
                        <a href="https://www.netaffinity.com" target="_blank"><img src="files/images/logo1.png" alt="Net Affinity" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div><!--footerBottom-->

</footer>

<script type="text/javascript" src="files/javascripts/plugins/plugins.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.superslides.min.js"></script>
<script type="text/javascript" src="files/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="files/javascripts/owl.carousel.min.js"></script>

<script type="text/javascript" src="files/javascripts/wow.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.heapbox.min.js"></script>
<script type="text/javascript" src="files/javascripts/searchbox.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="files/javascripts/custom.js"></script>
<!--Searchbox CSS-->
<link rel="stylesheet" href="https://secure.mespilhotel.com/files/css/searchbox.scss">
<link href="files/css/box-searchbox-fix.css" rel="stylesheet" type="text/css"/>
<!--Searchbox JS-->
<script type="text/javascript" src="https://secure.mespilhotel.com/files/js/searchbox.js"></script>


<!--  -->



</body>
</html>