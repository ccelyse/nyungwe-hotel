<footer class="full-width">
    <div class="footer-nav hidden-lg hidden-md text-center">
        <img src="files/images/menu.png" alt="" class="footer-menu-t">
        <nav class="full-width footer-menu display-none">

            <ul>

                <li><a href="">Reviews</a></li>
                <li><a href="">Sitemap</a></li>
                <li><a href="">Privacy Policy</a></li>
                <li><a href="" target="_blank">Brochure</a></li>
            </ul>
        </nav>
    </div>
    <div class="footerTop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="specialOffer visible-lg-inline-block"><a href="">Sign up for Special Offers</a></p>
                    <nav class="footerMenu hidden-sm hidden-xs">

                        <ul>
                            <li><a href="">Reviews</a></li>
                            <li><a href="">Sitemap</a></li>
                            <li><a href="">Privacy Policy</a></li>
                            <li><a href="" target="_blank">Brochure</a></li>
                        </ul>
                    </nav>
                    <div class="social">
                        <span>FOLLOW US</span>
                        <ul style="margin-top: 10px">
                            <li><a href=""><i class="fa fa-facebook fa-2x"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter fa-2x"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram fa-2x"></i></a></li>
                            <a href=""><img style="max-height: 50px;
                            position: absolute;
                            bottom: -2px;
                            padding-left: 10px;" src="files/images/tripadvisor.png" /></a>
                            <!--<li class="fb"><a href="//www.facebook.com/pages/Dublin-Ireland/Mespil-Hotel/61218316670" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li class="twitter"><a href="//twitter.com/MespilHotel" target="_blank"></a></li>
                            <li class="google"><a href="https://plus.google.com/108877475206796989243/posts" target="_blank"></a></li>
                            <li class="instagram"><a href="https://www.instagram.com/mespilhoteldublin/" target="_blank"></a></li>-->

                        </ul>
                    </div>
                    <p class="copyright hidden-lg hidden-md">©2018 Nyungwe Top View Hotel</p>
                    <div class="row hidden-lg hidden-md">
                        <div class="footer-mb-ph col-sm-6 col-xs-12 text-right">Phone : +250787109335</div>
                        <div class="footer-mb-em col-sm-6 col-xs-12 text-left">Email : <a href="mailto:mespil@leehotels.com">reservations@nyungwehotel.com</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--footerTop-->
    <div class="footerBottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyfix">
                        <p class="copyright hidden-sm hidden-xs">©2018 Nyungwe Top View Hote</p>
                        <ul class="hidden-sm hidden-xs">
                            <li>Phone : +250787109335</li>
                            <li>Email : <a href="mailto:mespil@leehotels.com">reservations@nyungwehotel.com</a></li>
                        </ul>
                    </div>
                    <div class="logo2">

                        <!-- GOOGLE LANGUAGES -->
                        <div id="google_translate_element"></div>
                        <!--
                        <script type="text/javascript">
                        function googleTranslateElementInit() {
                        new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                    }
                </script>
                 -->
                        <!-- <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> -->
                        <!-- GOOGLE LANGUAGES -->

                        {{--<a href="https://www.sligoparkhotel.com/" target="_blank"><img src="files/images/logo3.png" alt="Lee Sligo Park" /></a>--}}
                        {{--<a href="https://www.leehotels.com/" target="_blank"><img src="files/images/logo2.png" alt="Lee Hotels" /></a>--}}
                        {{--<a href="https://www.netaffinity.com" target="_blank"><img src="files/images/logo1.png" alt="Net Affinity" /></a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div><!--footerBottom-->

</footer>