<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <!-- Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-decetion" content="telephone=no"/>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="canonical" href="index.html">

    <!-- links -->
    <link rel="shortcut icon" type="image/x-icon" href="files/images/icon.png" >

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="files/css/searchbox.scss.css">
    <link href="files/css/box-searchbox-fix.css" rel="stylesheet" type="text/css"/>
    <link href="files/stylesheets/styles.css" type="text/css" rel="stylesheet" />

    <meta name="msvalidate.01" content="38F7A42322AA18E2A0A321D7CB0A70FF"/></head>
<body class="home" style="opacity:0;">


<header class="header wow fadeInDown" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">
                <div class="logo">
                    <a href="{{url('/')}}"><img src="files/images/logo.jpg" alt="logo" /></a>
                    <!-- BEGIN OF SWING TAG -->


                    <style>.gift-tag-swing{
                            display: inline-block;
                            position:absolute;
                            top: 100px;
                            left: 50%;
                            margin-left: -86px;
                            z-index: -2;
                            background-color:transparent !important
                        }
                        header.fixed .gift-tag-swing{
                            opacity: 0;
                            visibility: hidden;
                        }

                        @media (max-width: 767px){
                            .gift-tag-swing{
                                display:none !important;
                            }

                        }
                        @media (min-width: 768px){
                            .gift-tag-swing{
                                top: 82px;
                                right: 50px;
                                left: auto;
                            }

                        }
                        @media (min-width: 992px){
                            .gift-tag-swing{
                                top: 74px;
                                left: 42px;
                                margin-left: 0;
                            }
                            header.fixed .gift-tag-swing{
                                top: 32px;
                                left: 42px;
                                margin-left: 0;
                            }
                        }
                        @media (min-width: 1200px){
                            .gift-tag-swing{
                                top:94px;
                                left: 4px;
                                margin-left:-28px;
                            }
                            header.fixed .gift-tag-swing{
                                top: 31px;
                                left: 10px;
                                margin-left: 0;
                            }
                        }
                        @-webkit-keyframes swingimage{
                            0%{-webkit-transform:rotate(10deg)}
                            50%{-webkit-transform:rotate(-5deg)}
                            100%{-webkit-transform:rotate(10deg)}
                        }
                        @keyframes swinging{
                            0%{transform:rotate(10deg)}
                            50%{transform:rotate(-5deg)}
                            100%{transform:rotate(10deg)}
                        }
                        .swingimage{
                            -webkit-transform-origin:50% 0;
                            transform-origin:50% 0;
                            -webkit-animation:swinging 3.5s ease-in-out forwards infinite;
                            animation:swinging 3.5s ease-in-out forwards infinite
                        }
                    </style>



                    <!--<a class="gift-tag-swing" href="https://secure.mespilhotel.com/bookings/specials/winter-sale">
                        <img class="swingimage" src="/files/images/swinging-tag.png" alt="swing tag voucher image" />
                    </a>-->

                    <!-- END OF SWING TAG -->                </div>
                <div class="headerTop hidden-sm hidden-xs">
                    <span class="phone"> Login</span>
                    <span class="phone"><i class="zmdi zmdi-hc-fw zmdi-phone"></i> +250 787 109 335</span>

                    <div class="clearfix"></div>
                    <nav class="mainMenu">
                        <ul>
                            <li class="first"><a href="{{url('Accommodation')}}">Accommodations</a>
                            </li>
                            <li class="first"><a href="{{url('AboutUs')}}">About Us</a>
                            </li>
                            <li><a href="{{url('Facilities')}}">Facilities</a>
                            </li>
                            <li><a href="">Gallery</a></li>
                            <li><a href="">Contact</a>

                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="book-box hidden-sm hidden-xs">
                    <form method="post" action="" id="bke_searchbox_data" style="display:none;" >
                        <input type="hidden" value="1" id="bke_minstay">
                        <input type="hidden" value="364" id="bke_maxstay">
                        <input type="hidden" name="bke_form_token" value="" id="bke_form_token">
                        <input type="hidden" name="grid_view" value="2" id="bke_grid_view">
                        <input type="hidden" name="lang" value="en" id="bke_lang">
                        <input type="hidden" name="bke_ratecode" value="" id="bke_ratecode">
                        <input type="hidden" name="bke_arrival_day" value="01" id="bke_arrival_day">
                        <input type="hidden" name="bke_arrival_month" value="01" id="bke_arrival_month">
                        <input type="hidden" name="bke_arrival_year" value="2011" id="bke_arrival_year">
                        <input type="hidden" name="bke_departure_day" value="" id="bke_departure_day">
                        <input type="hidden" name="bke_departure_month" value="" id="bke_departure_month">
                        <input type="hidden" name="bke_departure_year" value="" id="bke_departure_year">
                        <input type="hidden" name="bke_nights" value="1" id="bke_nights">

                    </form>
                    <div class="na-searchbox">
                        <div class="na-searchbox__parameters">
                            <input type="hidden" name="searchbox_type" value="">
                            <input type="hidden" name="searchbox_display" value="boxes">
                            <input type="hidden" name="searchbox_size" value="">
                            <input type="hidden" name="searchbox_flexdate" value="">
                            <input type="hidden" name="searchbox_usp" value="">
                            <input type="hidden" name="searchbox_collapse" value="yes">
                            <input type="hidden" name="searchbox_ratecode" value="">
                            <input type="hidden" name="searchbox_hidden" value="no">
                            <input type="hidden" name="searchbox_debug" value="">
                            <input type="hidden" name="searchbox_location" value="">
                        </div>
                        <div class="na-searchbox__header">
                            Book Your Stay
                        </div>
                        <div class="na-searchbox__main">

                            <div class="na-searchbox__dates">
                                <div class="na-searchbox__checkin">
                                    <span class="na-searchbox__label">Check in</span>
                                    <div class="na-searchbox__day-week"></div>
                                    <div class="na-searchbox__day-number">01</div>
                                    <div class="na-searchbox__month">01</div>
                                    <div class="na-searchbox__year">2019</div>
                                </div>
                            </div>
                            <!--if bke_ratecode_section-->
                            <div class="na-searchbox__ratecode">
                                <input type="text" class="na-searchbox__ratecode-input" name="ratecode" placeholder="Have a promo code?">
                            </div>
                            <!--endif bke_ratecode_section-->
                            <button class="na-searchbox__submit">Search</button>
                        </div>
                        <!-- IF bke_usp -->
                        <div class="na-searchbox__usptrigger">
                            Why Book With Us? <img src="files/images/usp-tick.svg" alt="Mespil Hotel">
                        </div>
                        <!-- ENDIF bke_usp -->
                        <!-- IF bke_usp -->
                        <div class="na-searchbox__uspoverlay">
                            <div class="na-searchbox__uspcontainer">
                                <div class="na-searchbox__uspclose"><i class="zmdi zmdi-close-circle-o"></i></div>
                                <div class="na-searchbox__uspheading">Why Book With Us? <img src="files/images/usp-tick.svg" alt="Mespil Hotel"></div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="files/images/usp-tick.svg" alt="Best Price Guarantee">
                                    <span class="na-searchbox__usptitle">Best Price Guarantee</span>
                                    <span class="na-searchbox__usptext">Best rate available online!</span>
                                </div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="files/images/usp-tick.svg" alt="No Booking Fee">
                                    <span class="na-searchbox__usptitle">No Booking Fee</span>
                                    <span class="na-searchbox__usptext">Book now, pay later.</span>
                                </div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="files/images/usp-tick.svg" alt="Free Wi-Fi">
                                    <span class="na-searchbox__usptitle">Free Wi-Fi</span>
                                    <span class="na-searchbox__usptext">500mb High Speed Wi-Fi available throughout the hotel!</span>
                                </div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="files/images/usp-tick.svg" alt="Free Cancellation">
                                    <span class="na-searchbox__usptitle">Free Cancellation</span>
                                    <span class="na-searchbox__usptext">Up to 24 hours prior</span>
                                </div>

                                <div class="na-searchbox__usplistitem">
                                    <img src="files/images/usp-tick.svg" alt="Fitness Suite">
                                    <span class="na-searchbox__usptitle">Fitness Suite</span>
                                    <span class="na-searchbox__usptext">Complimentary access! (18+)</span>
                                </div>

                            </div>
                        </div>
                        <!-- ENDIF bke_usp -->
                        <div class="na-searchbox__overlay">
                            <div class="na-searchbox__overlaylogo">
                                <div class="na-searchbox__overlaylogowrapper">
                                    <img src="https://secure.mespilhotel.com/bookings/showimage?id=21B785F10889100848E2485F66B4B250&amp;type=18" alt="Mespil Hotel"/>
                                </div>
                                <div class="na-searchbox__overlayclose"><i class="zmdi zmdi-close-circle-o"></i></div>
                            </div>
                            <div class="na-searchbox__overlaywrapper">
                                <div class="na-searchbox__overlayscroll">
                                    <div class="na-searchbox__header">
                                        Book Your Stay
                                    </div>
                                    <!-- IF bke_usp -->
                                    <div class="na-searchbox__overlayusp">
                                        <div class="na-searchbox__overlayuspwrapper">
                                            <div class="na-searchbox__overlayuspheading">Why Book With Us?</div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="files/images/usp-tick.svg" alt="Best Price Guarantee">
                                                <span class="na-searchbox__overlayusptitle">Best Price Guarantee</span>
                                                <span class="na-searchbox__overlayusptext">Best rate available online!</span>
                                            </div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="files/images/usp-tick.svg" alt="No Booking Fee">
                                                <span class="na-searchbox__overlayusptitle">No Booking Fee</span>
                                                <span class="na-searchbox__overlayusptext">Book now, pay later.</span>
                                            </div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="files/images/usp-tick.svg" alt="Free Wi-Fi">
                                                <span class="na-searchbox__overlayusptitle">Free Wi-Fi</span>
                                                <span class="na-searchbox__overlayusptext">500mb High Speed Wi-Fi available throughout the hotel!</span>
                                            </div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="files/images/usp-tick.svg" alt="Free Cancellation">
                                                <span class="na-searchbox__overlayusptitle">Free Cancellation</span>
                                                <span class="na-searchbox__overlayusptext">Up to 24 hours prior</span>
                                            </div>

                                            <div class="na-searchbox__overlayusplistitem">
                                                <img src="files/images/usp-tick.svg" alt="Fitness Suite">
                                                <span class="na-searchbox__overlayusptitle">Fitness Suite</span>
                                                <span class="na-searchbox__overlayusptext">Complimentary access! (18+)</span>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- ENDIF bke_usp -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile-element pull-right visible-sm visible-xs">
                    <div class="mb-nav-btn">
                        <img src="files/images/menu.png" alt="">
                    </div>
                    <a href="tel:+35314884600">
                        <div class="mb-ph">
                            <i class="zmdi zmdi-hc-fw zmdi-phone"></i> <br>
                            <span>CALL</span>
                            <div class="ph-hd"> +250 787 109 335</div>
                            <div class="ph-hd"> Login</div>
                        </div>
                    </a>
                    <div class="book-mb">
                        <div class="book-hd open-searchbox-02">
                            <span class="hidden-sm hidden-xs">Book Now</span>
                            <span class="hidden-md hidden-lg">Book Now</span>
                            <i class="fa fa-calendar-check-o"></i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>
<nav class="mobile-nav">
    <img class="closeMenu" src="files/images/closeMenu.png" alt="">
    <div class="container">
        <ul>
            <li class="visible-sm visible-xs"><a href="{{url('/')}}"><img src="files/images/logo.jpg" alt="logo" /></a> </li>
        </ul>

        <ul>
            <li class="first"><a href="{{url('Accommodation')}}">Accommodations</a>
            </li>
            <li class="first"><a href="{{url('AboutUs')}}">About Us</a>
            </li>
            <li><a href="{{url('Facilities')}}">Facilities</a>
            </li>
            <li><a href="">Gallery</a></li>
            <li><a href="">Contact</a>

            </li>
        </ul>
    </div>
</nav>

<form method="post" action="https://secure.mespilhotel.com/bookings/checkavailability" id="bke_searchbox_data" style="display:none;" >
    <input type="hidden" value="1" id="bke_minstay">
    <input type="hidden" value="364" id="bke_maxstay">
    <input type="hidden" name="bke_form_token" value="" id="bke_form_token">
    <input type="hidden" name="grid_view" value="2" id="bke_grid_view">
    <input type="hidden" name="lang" value="en" id="bke_lang">
    <input type="hidden" name="bke_ratecode" value="" id="bke_ratecode">
    <input type="hidden" name="bke_arrival_day" value="31" id="bke_arrival_day">
    <input type="hidden" name="bke_arrival_month" value="10" id="bke_arrival_month">
    <input type="hidden" name="bke_arrival_year" value="2018" id="bke_arrival_year">
    <input type="hidden" name="bke_departure_day" value="" id="bke_departure_day">
    <input type="hidden" name="bke_departure_month" value="" id="bke_departure_month">
    <input type="hidden" name="bke_departure_year" value="" id="bke_departure_year">
    <input type="hidden" name="bke_nights" value="1" id="bke_nights">

</form>
<div class="na-searchbox">
    <div class="na-searchbox__parameters">
        <input type="hidden" name="searchbox_type" value="">
        <input type="hidden" name="searchbox_display" value="overlay">
        <input type="hidden" name="searchbox_size" value="">
        <input type="hidden" name="searchbox_flexdate" value="">
        <input type="hidden" name="searchbox_usp" value="">
        <input type="hidden" name="searchbox_collapse" value="">
        <input type="hidden" name="searchbox_ratecode" value="">
        <input type="hidden" name="searchbox_hidden" value="yes">
        <input type="hidden" name="searchbox_debug" value="">
        <input type="hidden" name="searchbox_location" value="">
    </div>
    <div class="na-searchbox__header">
        Book Your Stay
    </div>
    <div class="na-searchbox__main">

        <div class="na-searchbox__dates">
            <div class="na-searchbox__checkin">
                <span class="na-searchbox__label">Check in</span>
                <div class="na-searchbox__day-week"></div>
                <div class="na-searchbox__day-number">01</div>
                <div class="na-searchbox__month">01</div>
                <div class="na-searchbox__year">2019</div>
            </div>
        </div>
        <!--if bke_ratecode_section-->
        <div class="na-searchbox__ratecode">
            <input type="text" class="na-searchbox__ratecode-input" name="ratecode" placeholder="Have a promo code?">
        </div>
        <!--endif bke_ratecode_section-->
        <button class="na-searchbox__submit">Search</button>
    </div>
    <!-- IF bke_usp -->
    <div class="na-searchbox__usptrigger">
        Why Book With Us? <img src="files/images/usp-tick.svg" alt="Mespil Hotel">
    </div>
    <!-- ENDIF bke_usp -->
    <!-- IF bke_usp -->
    <div class="na-searchbox__uspoverlay">
        <div class="na-searchbox__uspcontainer">
            <div class="na-searchbox__uspclose"><i class="zmdi zmdi-close-circle-o"></i></div>
            <div class="na-searchbox__uspheading">Why Book With Us? <img src="files//images/usp-tick.svg" alt="Mespil Hotel"></div>

            <div class="na-searchbox__usplistitem">
                <img src="files/images/usp-tick.svg" alt="Best Price Guarantee">
                <span class="na-searchbox__usptitle">Best Price Guarantee</span>
                <span class="na-searchbox__usptext">Best rate available online!</span>
            </div>

            <div class="na-searchbox__usplistitem">
                <img src="files/images/usp-tick.svg" alt="No Booking Fee">
                <span class="na-searchbox__usptitle">No Booking Fee</span>
                <span class="na-searchbox__usptext">Book now, pay later.</span>
            </div>

            <div class="na-searchbox__usplistitem">
                <img src="files/images/usp-tick.svg" alt="Free Wi-Fi">
                <span class="na-searchbox__usptitle">Free Wi-Fi</span>
                <span class="na-searchbox__usptext">500mb High Speed Wi-Fi available throughout the hotel!</span>
            </div>

            <div class="na-searchbox__usplistitem">
                <img src="files/images/usp-tick.svg" alt="Free Cancellation">
                <span class="na-searchbox__usptitle">Free Cancellation</span>
                <span class="na-searchbox__usptext">Up to 24 hours prior</span>
            </div>

            <div class="na-searchbox__usplistitem">
                <img src="files/images/usp-tick.svg" alt="Fitness Suite">
                <span class="na-searchbox__usptitle">Fitness Suite</span>
                <span class="na-searchbox__usptext">Complimentary access! (18+)</span>
            </div>

        </div>
    </div>
    <!-- ENDIF bke_usp -->
    <div class="na-searchbox__overlay">
        <div class="na-searchbox__overlaylogo">
            <div class="na-searchbox__overlaylogowrapper">
                <img src="https://secure.mespilhotel.com/bookings/showimage?type=18&amp;id=21B785F10889100848E2485F66B4B250" alt="Mespil Hotel"/>
            </div>
            <div class="na-searchbox__overlayclose"><i class="zmdi zmdi-close-circle-o"></i></div>
        </div>
        <div class="na-searchbox__overlaywrapper">
            <div class="na-searchbox__overlayscroll">
                <div class="na-searchbox__header">
                    Book Your Stay
                </div>
                <!-- IF bke_usp -->
                <div class="na-searchbox__overlayusp">
                    <div class="na-searchbox__overlayuspwrapper">
                        <div class="na-searchbox__overlayuspheading">Why Book With Us?</div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="files/images/usp-tick.svg" alt="Best Price Guarantee">
                            <span class="na-searchbox__overlayusptitle">Best Price Guarantee</span>
                            <span class="na-searchbox__overlayusptext">Best rate available online!</span>
                        </div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="files/images/usp-tick.svg" alt="No Booking Fee">
                            <span class="na-searchbox__overlayusptitle">No Booking Fee</span>
                            <span class="na-searchbox__overlayusptext">Book now, pay later.</span>
                        </div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="files/images/usp-tick.svg" alt="Free Wi-Fi">
                            <span class="na-searchbox__overlayusptitle">Free Wi-Fi</span>
                            <span class="na-searchbox__overlayusptext">500mb High Speed Wi-Fi available throughout the hotel!</span>
                        </div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="files/images/usp-tick.svg" alt="Free Cancellation">
                            <span class="na-searchbox__overlayusptitle">Free Cancellation</span>
                            <span class="na-searchbox__overlayusptext">Up to 24 hours prior</span>
                        </div>

                        <div class="na-searchbox__overlayusplistitem">
                            <img src="files/images/usp-tick.svg" alt="Fitness Suite">
                            <span class="na-searchbox__overlayusptitle">Fitness Suite</span>
                            <span class="na-searchbox__overlayusptext">Complimentary access! (18+)</span>
                        </div>

                    </div>
                </div>
                <!-- ENDIF bke_usp -->
            </div>
        </div>
    </div>
</div>

<!--Header End-->

<!-- 7835 -->
@yield('content')