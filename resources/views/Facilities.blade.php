@extends('layouts.master')

@section('title', 'Nyungwe Hotel')

@section('content')


<div class="slider-height" style="background-color: #000;">
    <div id="slides" class="home-slider superslides full-width">
        <div class="slides-container">

            <div class="slide-item">
                <img class="hero-image" src="cmsImages/DJI_0031.jpg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>
                                Our Facilities
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide-item">
                <img class="hero-image" src="cmsImages/DJI_0031.jpg" alt="">
                <div class="container">
                    <div class="verticalCenter slide-content">
                        <div class="verticalInner">
                            <h2>
                                Our Facilities
                            </h2>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <nav class="slides-navigation">
            <a href="index.html#" class="next"></a>
            <a href="index.html#" class="prev"></a>
        </nav>
        <a class="nextblock wow fadeInDown"  data-wow-delay="0.5s" href="index.html#Next-block">
            <div class="verticalCenter">
                <div class="verticalInner">
                    <span>Explore</span>
                    <i class="fa fa-angle-down"></i>
                </div>
            </div>
        </a>
    </div>
    <!--Slider End-->

</div>


<section class="featuers full-width">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <!-- ITEM 1 -->
                <div class="ft-big ft-big-1 text-center marginB-30 wow fadeInLeft" style="background-image: url('cmsImages/DJI_0032 .jpg');">
                    <div class="ft-big-inner">
                        <h3>Ground Shuttle within the country from or to Nyungwe National Park or other destination as requested by the Guest</h3>

                        {{--<a href="" target="_self" class="more-content">Read More</a>--}}
                    </div>
                </div>

                <!-- ITEM 2 -->
                <div class="ft-sm  wow fadeInDown">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ft-sm-img ft-sm-img-2 eqImg" style="background-image: url('cmsImages/UOL-71.jpg');"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ft-sm-text eqImg">
                                <h3>Conference Room</h3>
                                {{--<p>Relaxing evening after exploring the Nyungwe national park, canopy walk and forest walk. </p>--}}
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-6">

                <!-- ITEM 3 -->
                <div class="ft-sm marginB-30 wow fadeInUp">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ft-sm-img ft-sm-img-1 eqImg" style="background-image: url('cmsImages/DSC_5044.jpg');"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ft-sm-text eqImg">
                                <h3>Front Desk Services</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-6">
                            <div class="ft-sm-img ft-sm-img-1 eqImg" style="background-image: url('cmsImages/carimg1.jpg');"></div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ft-sm-text eqImg">
                                <h3>Ticketing and reservation  </h3>
                            <p>Ticketing and reservation for Nyungwe related attractions as well as other attractions in the country including gorilla permits, Lake Kivu circuit tour </p>
                            <!--
                                <div class="big-featured-content full-width">
                                  <p>Serving breakfast, lunch and dinner in a tranquil setting overlooking the Grand Canal </p>
                                </div>
                                -->
                                {{--<a href="" target="_self" class="more-content">Read More</a>              </div>--}}
                            </div>
                        </div>
                    </div>

                <!-- ITEM 4 -->
                <div class="ft-big ft-big-2 text-center wow fadeInRight" style="background-image: url('cmsImages/DSC04731-2.jpg');">
                    <div class="ft-big-inner">
                        <h3>Restaurant and Bar</h3>
                        <!--
                                    -->
                        <a href="" target="_self" class="more-content">Read More</a>          </div>
                </div>




            </div>

        </div><!--row-->
    </div><!--container-->
</section>


<div class="mapBlock-wrap full-width">
    <h3 class="heading1">Our Location</h3>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d342282.55687988753!2d29.106133639126696!3d-2.565095567535544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x19c2e5687dabc83b%3A0x84af4fc4c78016a9!2sNyungwe+Forest+National+Park!5e0!3m2!1sen!2srw!4v1546804939775" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!--Map End-->

<style>@media (max-width: 767px) {
        .slide-item h2 {
            display: none;
        }
    }
</style>
@include('layouts.footer')



<!--[if IE]>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]><!-->
<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--<![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="files/javascripts/plugins/plugins.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.superslides.min.js"></script>
<script type="text/javascript" src="files/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="files/javascripts/owl.carousel.min.js"></script>

<script type="text/javascript" src="files/javascripts/wow.js"></script>

<script type="text/javascript" src="files/javascripts/jquery.heapbox.min.js"></script>
<!--Searchbox JS-->
<script type="text/javascript" src="https://secure.mespilhotel.com/files/js/searchbox.js"></script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyAdJTGnKU-sV1N04pZPCXBHJQhbQsUzR8g" type="text/javascript" ></script>





<script type="text/javascript">
    var map;
    var clicked = null;
    //var center_point =  new google.maps.LatLng(48.6739848,1.7337709);




    var json = [
        {
            "id": 1,
            "title": "<strong>The Mespil Hotel</strong><br> Mespil Road, Dublin 4, Ireland.<br>Phone: +353 (0)1 488 4600<br>Email: mespil@leehotels.com",
            "latitude": "53.33332",
            "longitude": "-6.24672",
            "category": "hotel",
            "icon":"files/images/mapIcons/hotelnew.png"
        },
        {
            "id": 2,
            "title": "St. Stephen's Green",
            "latitude": "53.338164",
            "longitude": "-6.259090",
            "category": "landmark",
            "icon":"files/images/mapIcons/stephensgreen.png"
        }, {
            "id": 3,
            "title": "Grafton Street",
            "latitude": "53.341883",
            "longitude": "-6.260026",
            "category": "shop",
            "icon":"files/images/mapIcons/grafton.png"
        }, {
            "id": 4,
            "title": "Royal Dublin Society",
            "latitude": "53.325731",
            "longitude": "-6.229699",
            "category": "venues",
            "icon":"files/images/mapIcons/royal.png"
        }, {
            "id": 5,
            "title": "Aviva Stadium",
            "latitude": "53.335219",
            "longitude": "-6.228489",
            "category": "venues",
            "icon":"files/images/mapIcons/aviva.png"
        }, {
            "id": 6,
            "title": "Temple Bar",
            "latitude": "53.345469",
            "longitude": "-6.264218",
            "category": "landmark",
            "icon":"files/images/mapIcons/templebar.png"
        }, {
            "id": 7,
            "title": "Trinity College",
            "latitude": "53.343848",
            "longitude": "-6.254616",
            "category": "landmark",
            "icon":"files/images/mapIcons/trinity.png"
        }, {
            "id": 8,
            "title": "Ballsbridge",
            "latitude": "53.329227",
            "longitude": "-6.231388",
            "category": "landmark",
            "icon":"files/images/mapIcons/ballsbridge.png"
        }, {
            "id": 9,
            "title": "National Concert Hall",
            "latitude": "53.334761",
            "longitude": "-6.259209",
            "category": "venues",
            "icon":"files/images/mapIcons/concerthall.png"
        }, {
            "id": 10,
            "title": "Board Gais Energy Theatre",
            "latitude": "53.344020",
            "longitude": "-6.240094",
            "category": "venues",
            "icon":"files/images/mapIcons/bordgais.png"
        }, {
            "id": 11,
            "title": "The 3areana",
            "latitude": "53.347443",
            "longitude": "-6.228643",
            "category": "venues",
            "icon":"files/images/mapIcons/3arena.png"
        }, {
            "id": 12,
            "title": "Convention Centre Dublin",
            "latitude": "53.347980",
            "longitude": "-6.239559",
            "category": "venues",
            "icon":"files/images/mapIcons/convention.png"
        }, {
            "id": 13,
            "title": "Shelbourne Race Track",
            "latitude": "53.340451",
            "longitude": "-6.230610",
            "category": "venues",
            "icon":"files/images/mapIcons/shelbourne.png"
        },


    ];
    var markers = [];
    var infowindow = new google.maps.InfoWindow();
    var myLatLng = {lat: 53.33500, lng:-6.24672};

    function initialize() {

        var styles =
            [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#444444"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                }
            ]




        var styledMap = new google.maps.StyledMapType(styles,
            {name: "Styled Map"});

        var ww = $(window).width();

        if(ww > 768){
            var Drag = true;
        }else{
            var Drag = false;
        }

        // Giving the map som options
        var mapOptions = {
            animation: google.maps.Animation.DROP,
            draggable: Drag,
            zoom: 15,
            scrollwheel: false,
            mapTypeControl:false,
            clickableIcons: false,
            streetViewControl: false,
            panControlOptions: {
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            center: myLatLng,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };
        // Creating the map
        map = new google.maps.Map(document.getElementById('map'), mapOptions);

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        directionsDisplay = new google.maps.DirectionsRenderer();
        directionsService = new google.maps.DirectionsService();
        directionsDisplay.setMap(map);
        directionsDisplay.setOptions({polylineOptions:{strokeColor:"#87b6ac",strokeWeight:5}, suppressMarkers:true })
        directionsDisplay.setPanel(document.getElementById("directions-load"));

        // Looping through all the entries from the JSON data
        for( var i = 0; i < json.length; i++ ) {

            // Current object
            var obj = json[i];
            var position =  new google.maps.LatLng(obj.latitude,obj.longitude);
            var marker_icon = obj.icon;
            var category1 = obj.category;
            //console.log(marker_icon);
            // Adding a new marker for the object
            //
            create_markers(obj,position,marker_icon,category1);


        } // end loop

        function create_markers(obj,position,marker_icon,category1){


            var marker = new google.maps.Marker({
                position: position,
                icon: marker_icon,
                category: category1,
                map: map,
                animation: google.maps.Animation.DROP,
                title: obj.title // this works, giving the marker a title with the correct title
            });

            markers.push(marker);



        }


        /*google.maps.event.addListener(map, 'click', function() {
            infowindow.close();
        });*/

        /**
         * Function to filter markers by category
         */

        filterMarkers = function (category1) {
            markers[0].setVisible(true);
            if(clicked == category1){
                for (i = 1; i < json.length; i++) {
                    marker = markers[i];
                    marker.setVisible(true);
                    clicked = null;
                }
            }else{
                clicked = category1;
                for (i = 1; i < json.length; i++) {
                    marker = markers[i];
                    // If is same category or category not picked
                    if (marker.category == category1 || category1.length === 0) {
                        marker.setVisible(true);
                    }
                    // Categories don't match
                    else {
                        marker.setVisible(false);
                    }
                }
            }
        }



        // Adding a new click event listener for the object
        function addClicker(marker, content, infowindow) {

            google.maps.event.addListener(marker, 'click', (function(marker, content, infowindow) {
                return function(){
                    infowindow.setContent(content);
                    infowindow.open(map,marker);
                };
            })(marker, content, infowindow));

        }

        for (var i = 0; i < Object.keys(markers).length; i++){
            //console.log(i, markers[i]);
            google.maps.event.trigger(markers[i], 'click');
        }
// google.maps.event.trigger(markers[1], 'click');

    }//

    function calcRoute() {
        var start = document.getElementById('dirFrom').value;
        var end = myLatLng;
        var request = {
            origin: start,
            destination: end,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function(response, status){
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            } else {
                alert("We could not find directions for your request, please check your From and To inputs then try again.");
            };
        });
        $('#directions-load').addClass('active');
    }

    google.maps.event.addDomListener(window, 'load', initialize);

    $(function() {

        $(".directions #dirFrom").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $('.directions .submit-button').click();
            }
        });

    });



</script>


<script type="text/javascript" src="files/javascripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="files/javascripts/custom.js"></script>

<!--  -->


</body>
</html>


@endsection